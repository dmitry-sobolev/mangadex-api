#![doc = include_str!("../README.md")]
#![forbid(unsafe_code)]

pub mod constants;
#[macro_use]
mod http_client;
pub mod error;
pub mod types;
pub mod v5;

use std::borrow::Cow;

use serde::Serialize;

pub use constants::*;
pub use error::Result;
pub use http_client::{HttpClient, HttpClientRef};
pub use v5::MangaDexClient;

#[cfg(all(feature = "chrono", feature = "time"))]
compile_error!("feature \"chrono\" and feature \"time\" cannot be enabled at the same time");

pub(crate) trait UrlSerdeQS {
    fn query_qs<T: Serialize>(self, query: &T) -> Self;
}

impl UrlSerdeQS for url::Url {
    fn query_qs<T: Serialize>(mut self, query: &T) -> Self {
        self.set_query(Some(
            &serde_qs::to_string(query).expect("failed to encode query string"),
        ));
        self
    }
}

pub(crate) trait FromResponse: Sized {
    type Response;

    fn from_response(res: Self::Response) -> Self;
}

pub(crate) trait Endpoint {
    type Query: Serialize;
    type Body: Serialize;
    type Response: FromResponse;

    fn method(&self) -> reqwest::Method {
        reqwest::Method::GET
    }

    fn path(&self) -> Cow<str>;

    fn require_auth(&self) -> bool {
        false
    }

    fn query(&self) -> Option<&Self::Query> {
        None
    }

    fn body(&self) -> Option<&Self::Body> {
        None
    }

    fn multipart(&self) -> Option<reqwest::multipart::Form> {
        None
    }
}

/// Helper macros for testing.
#[macro_use]
mod test_macros {
    /// Helper macro to construct an HttpClient for a given URI, either
    /// with or without authentication tokens.
    ///
    /// The arguments are:
    ///
    /// 1. `auth` or `no_auth`, indicating whether authentication tokens
    ///    are required or not.
    /// 2. Base URI for the HttpClient.
    #[allow(unused_macros)]
    #[cfg(test)]
    #[macro_export]
    macro_rules! build_http_client {
        { auth $uri:expr } => {
            HttpClient::builder()
                    .base_url($uri)
                .auth_tokens(AuthTokens {
                    session: "sessiontoken".to_string(),
                    refresh: "refreshtoken".to_string(),
                })
                    .build()
        };
        { no_auth $uri:expr } => {
            HttpClient::builder()
                    .base_url($uri)
                    .build()
        };
    }

    /// Helper macro to validate parameters sent in a request.
    ///
    /// The arguments are:
    ///
    /// 1. The location of the request payload.  Either `query_string` or `body`.
    /// 2. Server-side query params to check (camel-cased).
    /// 3. The request object.
    /// 4. A regex that a valid parameter value must match.
    ///
    /// A `body` payload is searched recursively for the query param, since in theory it
    /// could be nested arbitrarily deep.
    #[allow(unused_macros)]
    #[cfg(test)]
    #[macro_export]
    macro_rules! validate_params {
        { query_string $query_params:expr, $request:expr, $regex:expr } => {
            for param in $query_params {
                if
                    $request
                        .url
                        .query_pairs()
                        .find(|pair| {
                            pair.0.eq(param) && $regex.is_match(&pair.1)
                        }).is_none() {
                            return false;
                        }
            }
            true
        };
        { body $query_params:expr, $request:expr, $regex:expr } => {
            // recursively go through all sub-objects looking for the
            // param
            fn search_obj(param: &str, value: &Value, regex: &Regex) -> bool {
                match value.as_object() {
                    Some(obj) => {
                        for (k, v) in obj {
                            if k == param {
                                return regex.is_match(&v.as_str().unwrap());
                            }
                            if search_obj(param, &v, regex) {
                                return true;
                            }
                        }
                        return false;
                    },
                    _ => return false,
                }
            }

            let body: Value = serde_json::from_slice($request.body.as_slice()).unwrap();
            for param in $query_params {
                if !search_obj(param, &body, $regex) {
                    return false;
                }
            }
            true
        };
    }

    /// Helper macro to test date format sent to MangaDex.
    ///
    /// The arguments are:
    ///
    /// 1. Attribute-like structure containing payload location (`query_string` or `body`)
    ///    and optional `auth` identifier to say the request will contain authorization
    ///    headers.
    /// 2. Server-side query params to check (camel-cased).
    /// 3. A function to call to make the API call that contains the date parameters.
    ///
    /// A `body` payload is searched recursively for the query param, since in theory it
    /// could be nested arbitrarily deep.
    #[cfg(feature = "chrono")]
    #[cfg(test)]
    #[macro_export]
    macro_rules! test_serialize_mangadex_datetime {
        (#[$payload:ident] $query_params:expr, $client_setup:ident) => {
            crate::test_serialize_mangadex_datetime!(#[$payload no_auth] $query_params, $client_setup);
        };
        (#[$payload:ident $auth:ident] $query_params:expr, $client_setup:ident) => {
            #[tokio::test]
            async fn mangadex_date_format() -> anyhow::Result<()> {
                let mock_server = MockServer::start().await;
                let http_client: HttpClient = crate::build_http_client!{ $auth Url::parse(&mock_server.uri())? }?;
                let mangadex_client = MangaDexClient::new_with_http_client(http_client);

                let date_time_regex =
                    Regex::new(r"^\d{4}-[0-1]\d-([0-2]\d|3[0-1])T([0-1]\d|2[0-3]):[0-5]\d:[0-5]\d$")
                        .unwrap();

                let matcher = move |request: &Request| {
                    crate::validate_params! { $payload $query_params, request, &date_time_regex }
                };

                Mock::given(matcher)
                    .respond_with(ResponseTemplate::new(200))
                    .expect(1)
                    .mount(&mock_server)
                    .await;

                // Ignore error since we're only testing what the server sees
                let _ = $client_setup(&mangadex_client).await;

                Ok(())
            }
        };
    }
}
