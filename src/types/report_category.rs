use serde::{Deserialize, Serialize};

/// Mapping types to get the new UUIDs from the legacy, numerical, IDs.
#[derive(Debug, Serialize, Deserialize, Clone, Hash, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
#[non_exhaustive]
pub enum ReportCategory {
    Chapter,
    Manga,
    ScanlationGroup,
    User,
}

impl std::fmt::Display for ReportCategory {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        fmt.write_str(match self {
            Self::Chapter => "chapter",
            Self::Manga => "manga",
            Self::ScanlationGroup => "scanlation_group",
            Self::User => "user",
        })
    }
}
