use serde::{Deserialize, Serialize};

/// Used in the `related` field of a Manga relationships.
///
/// <https://api.mangadex.org/docs.html#section/Static-data/Manga-related-enum>
#[derive(Clone, Copy, Debug, Deserialize, Hash, PartialEq, PartialOrd, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum MangaRelation {
    AdaptedFrom,
    AlternateStory,
    BasedOn,
    Colored,
    Doujinshi,
    MainStory,
    Monochrome,
    Prequel,
    Preserialization,
    SameFranchise,
    Sequel,
    Serialization,
    SharedUniverse,
    SideStory,
    SpinOff,
}

impl std::fmt::Display for MangaRelation {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        let name = match self {
            Self::AdaptedFrom => "Adapted From",
            Self::AlternateStory => "Alternate Story",
            Self::BasedOn => "Based On",
            Self::Colored => "Colored",
            Self::Doujinshi => "Doujinshi",
            Self::MainStory => "Main Story",
            Self::Monochrome => "Monochrome",
            Self::Prequel => "Prequel",
            Self::Preserialization => "Preserialization",
            Self::SameFranchise => "Same Franchise",
            Self::Sequel => "Sequel",
            Self::Serialization => "Serialization",
            Self::SharedUniverse => "Shared Universe",
            Self::SideStory => "Side Story",
            Self::SpinOff => "Spin Off",
        };
        fmt.write_str(name)
    }
}
