//! Builder for the author creation endpoint.
//!
//! <https://api.mangadex.org/docs.html#operation/post-author>
//!
//! # Examples
//!
//! ```rust
//! use uuid::Uuid;
//!
//! use mangadex_api::v5::MangaDexClient;
//! use mangadex_api::types::{Password, Username};
//!
//! # async fn run() -> anyhow::Result<()> {
//! let client = MangaDexClient::default();
//!
//! let _login_res = client
//!     .auth()
//!     .login()
//!     .username(Username::parse("myusername")?)
//!     .password(Password::parse("hunter23")?)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! let author_id = Uuid::new_v4();
//! let res = client
//!     .author()
//!     .create()
//!     .name("Author Name")
//!     .version(1u32)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! println!("author create: {:?}", res);
//! # Ok(())
//! # }
//! ```

use derive_builder::Builder;
use serde::Serialize;
use url::Url;

use crate::v5::schema::{AuthorResponse, LocalizedString};
use crate::HttpClientRef;

#[derive(Debug, Serialize, Clone, Builder)]
#[serde(rename_all = "camelCase")]
#[builder(setter(into, strip_option), pattern = "owned")]
#[non_exhaustive]
pub struct CreateAuthor<'a> {
    /// This should never be set manually as this is only for internal use.
    #[serde(skip)]
    #[builder(pattern = "immutable")]
    pub http_client: HttpClientRef,

    pub name: &'a str,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub biography: Option<LocalizedString>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub twitter: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub pixiv: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub melon_book: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub fan_box: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub booth: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub nico_video: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub skeb: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub fantia: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub tumblr: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub youtube: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub website: Option<Url>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub version: Option<u32>,
}

endpoint! {
    POST ("/author"),
    #[body auth] CreateAuthor<'_>,
    #[flatten_result] AuthorResponse
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "chrono")]
    use chrono::{DateTime, Utc};
    #[cfg(not(feature = "time"))]
    use fake::faker::chrono::en::DateTime;
    use fake::faker::lorem::en::Sentence;
    use fake::faker::name::en::Name;
    use fake::Fake;
    use serde_json::json;
    #[cfg(feature = "time")]
    use time::OffsetDateTime;
    use url::Url;
    use uuid::Uuid;
    use wiremock::matchers::{header, method, path};
    use wiremock::{Mock, MockServer, ResponseTemplate};

    use crate::types::MangaDexDateTime;
    use crate::v5::AuthTokens;
    use crate::{HttpClient, MangaDexClient};

    #[tokio::test]
    async fn create_author_fires_a_request_to_base_url() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .auth_tokens(AuthTokens {
                session: "sessiontoken".to_string(),
                refresh: "refreshtoken".to_string(),
            })
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let author_id = Uuid::new_v4();
        let author_name: String = Name().fake();
        let author_biography: String = Sentence(1..2).fake();

        let datetime = {
            #[cfg(not(any(feature = "chrono", feature = "time")))]
            let datetime: String = DateTime().fake();
            #[cfg(feature = "chrono")]
            let datetime: DateTime<Utc> = DateTime().fake();
            #[cfg(feature = "time")]
            let datetime = OffsetDateTime::now_utc();

            MangaDexDateTime::new(&datetime)
        };

        let _expected_body = json!({
            "name": author_name,
            "version": 1
        });
        let response_body = json!({
            "result": "ok",
            "response": "entity",
            "data": {
                "id": author_id,
                "type": "author",
                "attributes": {
                    "name": author_name,
                    "imageUrl": "",
                    "biography": {
                        "en": author_biography,
                    },
                    "twitter": null,
                    "pixiv": null,
                    "melonBook": null,
                    "fanBox": null,
                    "booth": null,
                    "nicoVideo": null,
                    "skeb": null,
                    "fantia": null,
                    "tumblr": null,
                    "youtube": null,
                    "website": null,
                    "version": 2,
                    "createdAt": datetime.to_string(),
                    "updatedAt": datetime.to_string(),

                },
                "relationships": [],
            }
        });

        Mock::given(method("POST"))
            .and(path(r"/author"))
            .and(header("Authorization", "Bearer sessiontoken"))
            .and(header("Content-Type", "application/json"))
            // TODO: Make the request body check work.
            // .and(body_json(expected_body))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let _ = mangadex_client
            .author()
            .create()
            .name(author_name.as_str())
            .version(1u32)
            .build()?
            .send()
            .await?;

        Ok(())
    }
}
