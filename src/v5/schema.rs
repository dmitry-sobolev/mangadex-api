//! MangaDex API response object types.

mod at_home_server;
mod auth_tokens;
mod author;
mod chapter;
mod check_token_response;
mod cover;
mod custom_list;
mod error;
mod legacy_id_mapping;
mod login_response;
mod manga;
mod manga_aggregate;
mod manga_links;
mod manga_read_markers;
mod manga_reading_status;
mod manga_reading_statuses;
mod manga_relation;
mod refresh_token_response;
mod report;
mod scanlation_group;
mod tag;
#[cfg(feature = "upload")]
mod upload_session;
#[cfg(feature = "upload")]
mod upload_session_file;
mod user;

use std::collections::HashMap;

use serde::de::DeserializeOwned;
use serde::{Deserialize, Deserializer};
use uuid::Uuid;

use crate::error::Error;
use crate::types::{Language, MangaRelation, RelationshipType, ResponseType};
use crate::{FromResponse, Result};
pub(crate) use at_home_server::AtHomeServer;
pub(crate) use auth_tokens::AuthTokens;
pub(crate) use author::AuthorAttributes;
pub(crate) use chapter::ChapterAttributes;
pub(crate) use check_token_response::CheckTokenResponse;
pub(crate) use cover::CoverAttributes;
pub(crate) use custom_list::CustomListAttributes;
pub(crate) use error::MangaDexErrorResponse;
pub(crate) use legacy_id_mapping::LegacyMappingIdAttributes;
pub(crate) use login_response::LoginResponse;
pub(crate) use manga::MangaAttributes;
pub(crate) use manga_aggregate::MangaAggregate;
pub(crate) use manga_links::MangaLinks;
pub(crate) use manga_read_markers::{MangaReadMarkers, UngroupedMangaReadMarkers};
pub(crate) use manga_reading_status::MangaReadingStatus;
pub(crate) use manga_reading_statuses::MangaReadingStatuses;
pub(crate) use manga_relation::MangaRelationAttributes;
pub(crate) use refresh_token_response::RefreshTokenResponse;
pub(crate) use report::ReportReasonAttributes;
pub(crate) use scanlation_group::ScanlationGroupAttributes;
pub(crate) use tag::TagAttributes;
#[cfg(feature = "upload")]
pub(crate) use upload_session::UploadSessionResponse;
#[cfg(feature = "upload")]
pub(crate) use upload_session_file::{UploadSessionFileAttributes, UploadSessionFileData};
pub(crate) use user::UserAttributes;

/// Placeholder to hold response bodies that will be discarded.
///
/// `Result<()>` can't be used with the macro return type because it expects a unit type,
/// so a temporary struct is used.
///
/// # Examples
///
/// ```text
/// endpoint! {
///     POST "/captcha/solve",
///     #[body] SolveCaptcha<'_>,
///     #[discard_result] Result<NoData> // `Result<()>` results in a deserialization error despite discarding the result.
/// }
#[derive(Debug, Default, Deserialize, Clone, Hash, PartialEq, Eq)]
pub struct NoData;

pub type LocalizedString = HashMap<Language, String>;

/// Deserializer helper to handle JSON array or object types.
///
/// MangaDex currently returns an empty array when the localized string field isn't present.
pub(crate) mod localizedstring_array_or_map {
    use std::collections::HashMap;

    use serde::de::{Deserialize, Deserializer, MapAccess, SeqAccess, Visitor};

    use super::LocalizedString;

    pub fn deserialize<'de, D>(deserializer: D) -> Result<LocalizedString, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct V;

        impl<'de> Visitor<'de> for V {
            type Value = LocalizedString;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("array or object")
            }

            fn visit_seq<A>(self, mut _seq: A) -> Result<Self::Value, A::Error>
            where
                A: SeqAccess<'de>,
            {
                Ok(HashMap::new())
            }

            fn visit_map<A>(self, map: A) -> Result<Self::Value, A::Error>
            where
                A: MapAccess<'de>,
            {
                let de = serde::de::value::MapAccessDeserializer::new(map);
                let helper = LocalizedString::deserialize(de)?;
                Ok(helper)
            }
        }

        deserializer.deserialize_any(V)
    }
}

pub type AtHomeServerResponse = Result<AtHomeServer>;

pub type AuthorObject = ApiObject<AuthorAttributes>;
pub type AuthorData = ApiData<AuthorObject>;
pub type AuthorResponse = Result<AuthorData>;
pub type AuthorListResponse = Result<Results<AuthorObject>>;

pub type ChapterObject = ApiObject<ChapterAttributes>;
pub type ChapterData = ApiData<ChapterObject>;
pub type ChapterResponse = Result<ChapterData>;
pub type ChapterListResponse = Result<Results<ChapterObject>>;

pub type CoverObject = ApiObject<CoverAttributes>;
pub type CoverData = ApiData<CoverObject>;
pub type CoverResponse = Result<CoverData>;
pub type CoverListResponse = Result<Results<CoverObject>>;

pub type CustomListObject = ApiObject<CustomListAttributes>;
pub type CustomListData = ApiData<CustomListObject>;
pub type CustomListResponse = Result<CustomListData>;
pub type CustomListListResponse = Result<Results<CustomListObject>>;

pub type GroupObject = ApiObject<ScanlationGroupAttributes>;
pub type GroupData = ApiData<GroupObject>;
pub type GroupResponse = Result<GroupData>;
pub type GroupListResponse = Result<Results<GroupObject>>;

pub type IdMappingObject = ApiObject<LegacyMappingIdAttributes>;
pub type IdMappingData = ApiData<IdMappingObject>;
pub type IdMappingListResponse = Result<Results<IdMappingObject>>;

pub type MangaObject = ApiObject<MangaAttributes>;
pub type MangaData = ApiData<MangaObject>;
pub type MangaResponse = Result<MangaData>;
pub type MangaListResponse = Result<Results<MangaObject>>;

pub type MangaAggregateResponse = Result<MangaAggregate>;

pub type UngroupedMangaReadMarkersResponse = Result<UngroupedMangaReadMarkers>;
pub type MangaReadMarkersResponse = Result<MangaReadMarkers>;

pub type MangaReadingStatusResponse = Result<MangaReadingStatus>;
pub type MangaReadingStatusesResponse = Result<MangaReadingStatuses>;

pub type MangaRelationObject = ApiObject<MangaRelationAttributes>;
pub type MangaRelationListResponse = Result<Results<MangaRelationObject>>;

pub type ReportReasonObject = ApiObjectNoRelationships<ReportReasonAttributes>;
pub type ReportReasonListResponse = Result<Results<ReportReasonObject>>;

pub type TagObject = ApiObject<TagAttributes>;
pub type TagData = ApiData<TagObject>;
pub type TagResponse = Result<TagData>;
pub type TagListResponse = Result<Results<TagObject>>;

#[cfg(feature = "upload")]
pub type UploadSessionFileObject = ApiObject<UploadSessionFileAttributes>;
#[cfg(feature = "upload")]
pub type UploadSessionFileResponse = Result<UploadSessionFileData<UploadSessionFileObject>>;

pub type UserObject = ApiObject<UserAttributes>;
pub type UserData = ApiData<UserObject>;
pub type UserResponse = Result<UserData>;
pub type UserListResponse = Result<Results<UserObject>>;

#[derive(Deserialize)]
#[serde(tag = "result", remote = "std::result::Result")]
enum ApiResultDef<T, E> {
    #[serde(rename = "ok")]
    Ok(T),
    #[serde(rename = "error")]
    Err(E),
}

#[derive(Deserialize)]
#[serde(bound = "T: DeserializeOwned, E: DeserializeOwned")]
pub(crate) struct ApiResult<T, E = MangaDexErrorResponse>(
    #[serde(with = "ApiResultDef")] std::result::Result<T, E>,
);

impl<T, E> ApiResult<T, E> {
    pub fn into_result(self) -> Result<T, E> {
        self.0
    }
}

/// API response for a single entity containing an [`ApiObject`] in the `data` field.
#[derive(Debug, Deserialize, Clone)]
pub struct ApiData<T> {
    pub response: ResponseType,
    pub data: T,
}

#[derive(Debug, Default, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ApiObject<A, T = RelationshipType> {
    pub id: Uuid,
    pub type_: T,
    pub attributes: A,
    pub relationships: Vec<Relationship>,
}

impl<A, T> FromResponse for ApiObject<A, T> {
    type Response = Self;

    fn from_response(value: Self::Response) -> Self {
        value
    }
}

#[derive(Debug, Default, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ApiObjectNoRelationships<A, T = RelationshipType> {
    pub id: Uuid,
    pub type_: T,
    pub attributes: A,
}

impl<A, T> FromResponse for ApiObjectNoRelationships<A, T> {
    type Response = Self;

    fn from_response(value: Self::Response) -> Self {
        value
    }
}

// TODO: Find a way to reduce the boilerplate for this.
// `struct-variant` (https://docs.rs/struct-variant) is a potential candidate for this.
#[derive(Debug, Deserialize, Clone)]
#[allow(clippy::large_enum_variant)]
#[serde(untagged)]
pub enum RelatedAttributes {
    /// Manga resource.
    Manga(MangaAttributes),
    /// Chapter resource.
    Chapter(ChapterAttributes),
    /// A Cover Art for a manga.
    ///
    /// On manga resources, only one cover art resource relation is returned,
    /// marking the primary cover if there are more than one. By default, this will be the latest
    /// volume's cover art. To see all the covers for a given manga, use the cover search endpoint.
    CoverArt(CoverAttributes),
    /// Author resource.
    Author(AuthorAttributes),
    /// ScanlationGroup resource.
    ScanlationGroup(ScanlationGroupAttributes),
    /// Tag resource.
    Tag(TagAttributes),
    /// User resource.
    User(UserAttributes),
    /// CustomList resource.
    CustomList(CustomListAttributes),
}

#[derive(Debug, Deserialize, Clone)]
pub struct Relationship {
    pub id: Uuid,
    #[serde(rename = "type")]
    pub type_: RelationshipType,
    /// Related Manga type.
    ///
    /// <https://api.mangadex.org/docs.html#section/Static-data/Manga-related-enum>
    ///
    /// This is only present for a Manga entity and a Manga relationship.
    pub related: Option<MangaRelation>,
    /// Contains object attributes for the type.
    ///
    /// Present if [Reference Expansion](https://api.mangadex.org/docs.html#section/Reference-Expansion) is applied.
    pub attributes: Option<RelatedAttributes>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Results<T> {
    pub response: ResponseType,
    pub data: Vec<T>,
    pub limit: u32,
    pub offset: u32,
    pub total: u32,
}

impl<T> FromResponse for Result<T, Error> {
    type Response = ApiResult<T, MangaDexErrorResponse>;

    fn from_response(value: Self::Response) -> Self {
        value.into_result().map_err(|e| e.into())
    }
}

impl<T> FromResponse for Vec<Result<T, Error>> {
    type Response = Vec<ApiResult<T, MangaDexErrorResponse>>;

    fn from_response(value: Self::Response) -> Self {
        value
            .into_iter()
            .map(|r| r.into_result().map_err(|e| e.into()))
            .collect()
    }
}

pub(crate) fn deserialize_null_default<'de, D, T>(deserializer: D) -> Result<T, D::Error>
where
    T: Default + Deserialize<'de>,
    D: Deserializer<'de>,
{
    let opt = Option::deserialize(deserializer)?;
    Ok(opt.unwrap_or_default())
}
