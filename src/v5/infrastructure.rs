//! Infrastructure endpoint handler.
//!
//! <https://api.mangadex.org/docs.html#tag/Infrastructure>

mod ping;

use crate::v5::infrastructure::ping::PingBuilder;
use crate::HttpClientRef;

/// Legacy endpoint handler builder.
#[derive(Clone, Debug)]
pub struct InfrastructureBuilder {
    http_client: HttpClientRef,
}

impl InfrastructureBuilder {
    pub fn new(http_client: HttpClientRef) -> Self {
        Self { http_client }
    }

    /// Ping the server.
    ///
    /// <https://api.mangadex.org/docs.html#tag/Infrastructure/paths/~1ping/get>
    pub fn ping(self) -> PingBuilder {
        PingBuilder::default().http_client(self.http_client)
    }
}
