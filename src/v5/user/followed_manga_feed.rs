//! Builder for the user manga feed endpoint to get a list of new chapters for a user's manga.
//!
//! <https://api.mangadex.org/docs.html#operation/get-user-follows-manga-feed>
//!
//! # Examples
//!
//! ```rust
//! use mangadex_api::v5::MangaDexClient;
//! use mangadex_api::types::{Password, Username};
//!
//! # async fn run() -> anyhow::Result<()> {
//! let client = MangaDexClient::default();
//!
//! let _login_res = client
//!     .auth()
//!     .login()
//!     .username(Username::parse("myusername")?)
//!     .password(Password::parse("hunter23")?)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! let res = client
//!     .user()
//!     .followed_manga_feed()
//!     .limit(1u32)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! println!("Manga feed: {:?}", res);
//! # Ok(())
//! # }
//! ```

use derive_builder::Builder;
use serde::Serialize;

use crate::types::{
    ContentRating, IncludeFutureUpdates, Language, MangaDexDateTime, MangaFeedSortOrder,
    ReferenceExpansionResource,
};
use crate::v5::schema::ChapterListResponse;
use crate::HttpClientRef;

#[derive(Debug, Serialize, Clone, Builder, Default)]
#[serde(rename_all = "camelCase")]
#[builder(setter(into, strip_option), pattern = "owned", default)]
#[non_exhaustive]
pub struct GetFollowedMangaFeed {
    /// This should never be set manually as this is only for internal use.
    #[serde(skip)]
    #[builder(pattern = "immutable")]
    pub http_client: HttpClientRef,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub offset: Option<u32>,
    #[builder(setter(each = "add_translated_language"))]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub translated_language: Vec<Language>,
    #[builder(setter(each = "add_original_language"))]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub original_language: Vec<Language>,
    #[builder(setter(each = "exclude_original_language"))]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub excluded_original_language: Vec<Language>,
    #[builder(setter(each = "add_content_rating"))]
    pub content_rating: Vec<ContentRating>,
    /// Flag to include future chapter updates in the results.
    ///
    /// Default: `IncludeFutureUpdates::Include` (1)
    pub include_future_updates: Option<IncludeFutureUpdates>,
    /// DateTime string with following format: `YYYY-MM-DDTHH:MM:SS`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created_at_since: Option<MangaDexDateTime>,
    /// DateTime string with following format: `YYYY-MM-DDTHH:MM:SS`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub updated_at_since: Option<MangaDexDateTime>,
    /// DateTime string with following format: `YYYY-MM-DDTHH:MM:SS`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub publish_at_since: Option<MangaDexDateTime>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub order: Option<MangaFeedSortOrder>,
    #[builder(setter(each = "include"))]
    pub includes: Vec<ReferenceExpansionResource>,
}

endpoint! {
    GET "/user/follows/manga/feed",
    #[query auth] GetFollowedMangaFeed,
    #[flatten_result] ChapterListResponse
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "chrono")]
    use chrono::{DateTime, Utc};
    #[cfg(not(feature = "time"))]
    use fake::faker::chrono::en::DateTime;
    use fake::faker::internet::en::Password;
    use fake::faker::name::en::Name;
    use fake::Fake;
    #[cfg(feature = "chrono")]
    use regex::Regex;
    use serde_json::json;
    #[cfg(feature = "time")]
    use time::OffsetDateTime;
    use url::Url;
    use uuid::Uuid;
    use wiremock::matchers::{header, method, path_regex};
    #[cfg(feature = "chrono")]
    use wiremock::Request;
    use wiremock::{Mock, MockServer, ResponseTemplate};

    use crate::types::MangaDexDateTime;
    use crate::v5::AuthTokens;
    use crate::{HttpClient, MangaDexClient};

    #[tokio::test]
    async fn get_followed_manga_feed_fires_a_request_to_base_url() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client: HttpClient = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .auth_tokens(AuthTokens {
                session: "sessiontoken".to_string(),
                refresh: "refreshtoken".to_string(),
            })
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let chapter_id = Uuid::new_v4();
        let uploader_id = Uuid::new_v4();
        let chapter_title: String = Name().fake();
        let hash: String = Password(16..24).fake();

        let datetime = {
            #[cfg(not(any(feature = "chrono", feature = "time")))]
            let datetime: String = DateTime().fake();
            #[cfg(feature = "chrono")]
            let datetime: DateTime<Utc> = DateTime().fake();
            #[cfg(feature = "time")]
            let datetime = OffsetDateTime::now_utc();

            MangaDexDateTime::new(&datetime)
        };

        let response_body = json!({
            "result": "ok",
            "response": "collection",
            "data": [
                {
                    "id": chapter_id,
                    "type": "chapter",
                    "attributes": {
                        "title": chapter_title,
                        "volume": "1",
                        "chapter": "1.5",
                        "translatedLanguage": "en",
                        "hash": hash,
                        "data": [
                            "1.jpg"
                        ],
                        "dataSaver": [
                            "1.jpg"
                        ],
                        "uploader": uploader_id,
                        "version": 1,
                        "createdAt": datetime.to_string(),
                        "updatedAt": datetime.to_string(),
                        "publishAt": datetime.to_string(),
                    },
                    "relationships": [],
                },
            ],
            "limit": 1,
            "offset": 0,
            "total": 1
        });

        Mock::given(method("GET"))
            .and(path_regex(r"/user/follows/manga/feed"))
            .and(header("Authorization", "Bearer sessiontoken"))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let _ = mangadex_client
            .user()
            .followed_manga_feed()
            .limit(1u32)
            .build()?
            .send()
            .await?;

        Ok(())
    }

    #[cfg(feature = "chrono")]
    async fn followed_manga_feed_datetime_setup(client: &MangaDexClient) {
        let date_time: DateTime<Utc> = DateTime().fake();
        let _ = client
            .user()
            .followed_manga_feed()
            .created_at_since(date_time)
            .updated_at_since(date_time)
            .publish_at_since(date_time)
            .build()
            .unwrap()
            .send()
            .await;
    }

    #[cfg(feature = "chrono")]
    crate::test_serialize_mangadex_datetime!(
        #[query_string auth]
        ["createdAtSince", "updatedAtSince", "publishAtSince"],
        followed_manga_feed_datetime_setup
    );
}
