//! Builder for the manga aggregate endpoint to get volumes and chapters.
//!
//! <https://api.mangadex.org/docs.html#tag/Manga/paths/~1manga~1{id}~1aggregate/get>
//!
//! # Examples
//!
//! ```rust
//! use uuid::Uuid;
//!
//! use mangadex_api::v5::MangaDexClient;
//!
//! # async fn run() -> anyhow::Result<()> {
//! let client = MangaDexClient::default();
//!
//! let manga_id = Uuid::new_v4();
//! let manga_res = client
//!     .manga()
//!     .aggregate()
//!     .manga_id(&manga_id)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! println!("manga aggregate: {:?}", manga_res);
//! # Ok(())
//! # }
//! ```

use derive_builder::Builder;
use serde::Serialize;
use uuid::Uuid;

use crate::types::Language;
use crate::v5::schema::MangaAggregateResponse;
use crate::HttpClientRef;

#[derive(Debug, Serialize, Clone, Builder)]
#[serde(rename_all = "camelCase")]
#[builder(setter(into, strip_option), pattern = "owned")]
#[non_exhaustive]
pub struct GetMangaAggregate<'a> {
    /// This should never be set manually as this is only for internal use.
    #[serde(skip)]
    #[builder(pattern = "immutable")]
    pub http_client: HttpClientRef,

    #[serde(skip)]
    pub manga_id: &'a Uuid,

    #[builder(setter(each = "add_language"), default)]
    pub translated_language: Vec<Language>,
    #[builder(setter(each = "add_group"), default)]
    pub groups: Vec<Uuid>,
}

endpoint! {
    GET ("/manga/{}/aggregate", manga_id),
    #[query] GetMangaAggregate<'_>,
    #[flatten_result] MangaAggregateResponse
}

#[cfg(test)]
mod tests {
    use serde_json::json;
    use url::Url;
    use uuid::Uuid;
    use wiremock::matchers::{method, path_regex};
    use wiremock::{Mock, MockServer, ResponseTemplate};

    use crate::{HttpClient, MangaDexClient};

    #[tokio::test]
    async fn legacy_id_mapping_fires_a_request_to_base_url() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client: HttpClient = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let manga_id = Uuid::new_v4();
        let chapter_id = Uuid::new_v4();
        let response_body = json!({
            "result": "ok",
            "volumes": {
                "1": {
                    "volume": "1",
                    "count": 2,
                    "chapters": {
                        "26": {
                            "chapter": "26",
                            "id": chapter_id,
                            "count": 2
                        }
                    }
                }
            }
        });

        Mock::given(method("GET"))
            .and(path_regex(r"/manga/[0-9a-fA-F-]+/aggregate"))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let res = mangadex_client
            .manga()
            .aggregate()
            .manga_id(&manga_id)
            .build()?
            .send()
            .await?;

        assert_eq!(res.volumes.len(), 1);

        let volume = res.volumes.get("1").unwrap();
        assert_eq!(volume.volume, "1");
        assert_eq!(volume.count, 2);

        assert_eq!(volume.chapters.len(), 1);
        let chapter = volume.chapters.get("26").unwrap();
        assert_eq!(chapter.chapter, "26");
        assert_eq!(chapter.id, chapter_id);
        assert_eq!(chapter.count, 2);

        Ok(())
    }
}
