//! Builder for the chapter update endpoint.
//!
//! <https://api.mangadex.org/docs.html#operation/put-chapter-id>
//!
//! # Examples
//!
//! ```rust
//! use std::collections::HashMap;
//!
//! use uuid::Uuid;
//!
//! use mangadex_api::types::Language;
//! use mangadex_api::v5::MangaDexClient;
//! use mangadex_api::types::{Password, Username};
//!
//! # async fn run() -> anyhow::Result<()> {
//! let client = MangaDexClient::default();
//!
//! let _login_res = client
//!     .auth()
//!     .login()
//!     .username(Username::parse("myusername")?)
//!     .password(Password::parse("hunter23")?)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! let manga_id = Uuid::new_v4();
//! let mut manga_titles = HashMap::new();
//! manga_titles.insert(Language::English, "Updated Manga Title".to_string());
//! let res = client
//!     .manga()
//!     .update()
//!     .manga_id(&manga_id)
//!     .title(manga_titles)
//!     .version(2u32)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! println!("update: {:?}", res);
//! # Ok(())
//! # }
//! ```

use derive_builder::Builder;
use serde::Serialize;
use uuid::Uuid;

use crate::types::{ContentRating, Demographic, Language, MangaLinks, MangaStatus, Tag};
use crate::v5::schema::{LocalizedString, MangaResponse};
use crate::HttpClientRef;

/// Update a manga's information.
///
/// All fields that are not changing should still have the field populated with the old information
/// so that it is not set as `null` on the server.
#[derive(Debug, Serialize, Clone, Builder)]
#[serde(rename_all = "camelCase")]
#[builder(setter(into, strip_option))]
#[non_exhaustive]
pub struct UpdateManga<'a> {
    /// This should never be set manually as this is only for internal use.
    #[serde(skip)]
    #[builder(pattern = "immutable")]
    pub http_client: HttpClientRef,

    #[serde(skip)]
    pub manga_id: &'a Uuid,

    #[builder(setter(each = "add_title"))]
    pub title: LocalizedString,
    #[builder(default)]
    pub alt_titles: Option<Vec<LocalizedString>>,
    #[builder(default)]
    pub description: Option<LocalizedString>,
    #[builder(default)]
    pub authors: Option<Vec<Uuid>>,
    #[builder(default)]
    pub artists: Option<Vec<Uuid>>,
    #[builder(default)]
    pub links: Option<MangaLinks>,
    #[builder(default)]
    pub original_language: Option<Language>,
    #[builder(default)]
    pub last_volume: Option<String>,
    #[builder(default)]
    pub last_chapter: Option<String>,
    #[builder(default)]
    pub publication_demographic: Option<Demographic>,
    #[builder(default)]
    pub status: Option<MangaStatus>,
    /// Year the manga was released.
    #[builder(default)]
    pub year: Option<u16>,
    #[builder(default)]
    pub content_rating: Option<ContentRating>,
    #[builder(default)]
    pub tags: Option<Vec<Tag>>,
    #[builder(default)]
    pub primary_cover: Option<Uuid>,
    pub version: u32,
}

endpoint! {
    PUT ("/manga/{}", manga_id),
    #[body auth] UpdateManga<'_>,
    #[flatten_result] MangaResponse
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "chrono")]
    use chrono::{DateTime, Utc};
    #[cfg(not(feature = "time"))]
    use fake::faker::chrono::en::DateTime;
    #[cfg(not(feature = "time"))]
    use fake::Fake;
    use serde_json::json;
    #[cfg(feature = "time")]
    use time::OffsetDateTime;
    use url::Url;
    use uuid::Uuid;
    use wiremock::matchers::{header, method, path_regex};
    use wiremock::{Mock, MockServer, ResponseTemplate};

    use crate::types::{Language, MangaDexDateTime};
    use crate::v5::AuthTokens;
    use crate::{HttpClient, MangaDexClient};

    #[tokio::test]
    async fn update_chapter_fires_a_request_to_base_url() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .auth_tokens(AuthTokens {
                session: "sessiontoken".to_string(),
                refresh: "refreshtoken".to_string(),
            })
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let manga_id = Uuid::new_v4();

        let datetime = {
            #[cfg(not(any(feature = "chrono", feature = "time")))]
            let datetime: String = DateTime().fake();
            #[cfg(feature = "chrono")]
            let datetime: DateTime<Utc> = DateTime().fake();
            #[cfg(feature = "time")]
            let datetime = OffsetDateTime::now_utc();

            MangaDexDateTime::new(&datetime)
        };

        let _expected_body = json!({
            "title": {
                "en": "New Manga Title"
            },
            "altTitles": null,
            "description": null,
            "authors": null,
            "artists": null,
            "links": null,
            "originalLanguage": null,
            "lastVolume": null,
            "lastChapter": null,
            "publicationDemographic": null,
            "status": null,
            "year": null,
            "contentRating": null,
            "tags": null,
            "modNotes": null,
            "version": 2
        });
        let response_body = json!({
            "result": "ok",
            "response": "entity",
            "data": {
                "id": manga_id,
                "type": "chapter",
                "attributes": {
                    "title": {
                        "en": "Test Manga"
                    },
                    "altTitles": [],
                    "description": {},
                    "isLocked": false,
                    "links": {},
                    "originalLanguage": "ja",
                    "lastVolume": "1",
                    "lastChapter": "1",
                    "publicationDemographic": "shoujo",
                    "status": "completed",
                    "year": 2021,
                    "contentRating": "safe",
                    "tags": [],
                    "state": "published",
                    "version": 2,
                    "createdAt": datetime.to_string(),
                    "updatedAt": datetime.to_string(),
                },
                "relationships": []
            }
        });

        Mock::given(method("PUT"))
            .and(path_regex(r"/manga/[0-9a-fA-F-]+"))
            .and(header("Authorization", "Bearer sessiontoken"))
            .and(header("Content-Type", "application/json"))
            // TODO: Make the request body check work.
            // .and(body_json(expected_body))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let _ = mangadex_client
            .manga()
            .update()
            .manga_id(&manga_id)
            .add_title((Language::English, "New Manga Title".to_string()))
            .version(2u32)
            .build()?
            .send()
            .await?;

        Ok(())
    }
}
