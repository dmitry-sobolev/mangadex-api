//! Builder for the manga feed endpoint to get a list of new chapters for a manga.
//!
//! <https://api.mangadex.org/docs.html#operation/get-manga-id-feed>
//!
//! # Examples
//!
//! ```rust
//! use uuid::Uuid;
//!
//! use mangadex_api::v5::MangaDexClient;
//!
//! # async fn run() -> anyhow::Result<()> {
//! let client = MangaDexClient::default();
//!
//! let manga_id = Uuid::new_v4();
//! let manga_res = client
//!     .manga()
//!     .feed()
//!     .manga_id(&manga_id)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! println!("Manga feed: {:?}", manga_res);
//! # Ok(())
//! # }
//! ```

use derive_builder::Builder;
use serde::Serialize;
use uuid::Uuid;

use crate::types::{
    ContentRating, IncludeFutureUpdates, Language, MangaDexDateTime, MangaFeedSortOrder,
    ReferenceExpansionResource,
};
use crate::v5::schema::ChapterListResponse;
use crate::HttpClientRef;

#[derive(Debug, Serialize, Clone, Builder)]
#[serde(rename_all = "camelCase")]
#[builder(setter(into, strip_option), pattern = "owned")]
#[non_exhaustive]
pub struct GetMangaFeed<'a> {
    /// This should never be set manually as this is only for internal use.
    #[serde(skip)]
    #[builder(pattern = "immutable")]
    pub http_client: HttpClientRef,

    #[serde(skip)]
    pub manga_id: &'a Uuid,

    // `manga_id` cannot use the `Default` trait so these attributes have to be manually set.
    #[builder(default)]
    pub limit: Option<u32>,
    #[builder(default)]
    pub offset: Option<u32>,
    #[builder(setter(each = "add_translated_language"), default)]
    pub translated_language: Vec<Language>,
    #[builder(setter(each = "add_original_language"), default)]
    pub original_language: Vec<Language>,
    #[builder(setter(each = "exclude_original_language"), default)]
    pub excluded_original_language: Vec<Language>,
    #[builder(setter(each = "add_content_rating"), default)]
    pub content_rating: Vec<ContentRating>,
    /// Flag to include future chapter updates in the results.
    ///
    /// Default: `IncludeFutureUpdates::Include` (1)
    #[builder(default)]
    pub include_future_updates: Option<IncludeFutureUpdates>,
    /// DateTime string with following format: `YYYY-MM-DDTHH:MM:SS`.
    #[builder(default)]
    pub created_at_since: Option<MangaDexDateTime>,
    /// DateTime string with following format: `YYYY-MM-DDTHH:MM:SS`.
    #[builder(default)]
    pub updated_at_since: Option<MangaDexDateTime>,
    /// DateTime string with following format: `YYYY-MM-DDTHH:MM:SS`.
    #[builder(default)]
    pub publish_at_since: Option<MangaDexDateTime>,
    #[builder(default)]
    pub order: Option<MangaFeedSortOrder>,
    #[builder(setter(each = "include"), default)]
    pub includes: Vec<ReferenceExpansionResource>,
}

endpoint! {
    GET ("/manga/{}/feed", manga_id),
    #[query] GetMangaFeed<'_>,
    ChapterListResponse
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "chrono")]
    use chrono::{DateTime, Utc};
    #[cfg(not(feature = "time"))]
    use fake::faker::chrono::en::DateTime;
    #[cfg(not(feature = "time"))]
    use fake::Fake;
    #[cfg(feature = "chrono")]
    use regex::Regex;
    use serde_json::json;
    #[cfg(feature = "time")]
    use time::OffsetDateTime;
    use url::Url;
    use uuid::Uuid;
    use wiremock::matchers::{method, path_regex};
    #[cfg(feature = "chrono")]
    use wiremock::Request;
    use wiremock::{Mock, MockServer, ResponseTemplate};

    use crate::types::MangaDexDateTime;
    use crate::{HttpClient, MangaDexClient};

    #[tokio::test]
    async fn get_manga_feed_fires_a_request_to_base_url() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client: HttpClient = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let manga_id = Uuid::new_v4();
        let chapter_id = Uuid::new_v4();
        let uploader_id = Uuid::new_v4();

        let datetime = {
            #[cfg(not(any(feature = "chrono", feature = "time")))]
            let datetime: String = DateTime().fake();
            #[cfg(feature = "chrono")]
            let datetime: DateTime<Utc> = DateTime().fake();
            #[cfg(feature = "time")]
            let datetime = OffsetDateTime::now_utc();

            MangaDexDateTime::new(&datetime)
        };

        let response_body = json!({
            "result": "ok",
            "response": "collection",
            "data": [
                {
                    "id": chapter_id,
                    "type": "chapter",
                    "attributes": {
                        "title": "Chapter title",
                        "volume": null,
                        "chapter": "1",
                        "translatedLanguage": "en",
                        "hash": "123456abcdef",
                        "data": [
                            "1.jpg"
                        ],
                        "dataSaver": [
                            "1.jpg"
                        ],
                        "uploader": uploader_id,
                        "version": 1,
                        "createdAt": datetime.to_string(),
                        "updatedAt": datetime.to_string(),

                        "publishAt": datetime.to_string(),
                    },
                    "relationships": []
                }
            ],
            "limit": 1,
            "offset": 0,
            "total": 1
        });

        Mock::given(method("GET"))
            .and(path_regex(r"/manga/[0-9a-fA-F-]+/feed"))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let _ = mangadex_client
            .manga()
            .feed()
            .manga_id(&manga_id)
            .limit(1u32)
            .build()?
            .send()
            .await?;

        Ok(())
    }

    #[cfg(feature = "chrono")]
    async fn manga_feed_datetime_setup(client: &MangaDexClient) {
        let date_time: DateTime<Utc> = DateTime().fake();
        let manga_id = Uuid::new_v4();
        let _ = client
            .manga()
            .feed()
            .manga_id(&manga_id)
            .created_at_since(date_time)
            .updated_at_since(date_time)
            .publish_at_since(date_time)
            .build()
            .unwrap()
            .send()
            .await;
    }

    #[cfg(feature = "chrono")]
    crate::test_serialize_mangadex_datetime!(
        #[query_string]
        ["createdAtSince", "updatedAtSince", "publishAtSince"],
        manga_feed_datetime_setup
    );
}
