//! Report endpoint handler.
//!
//! <https://api.mangadex.org/docs.html#tag/Report>

mod create;
pub(crate) mod list;

use crate::v5::report::create::CreateReportBuilder;
use crate::v5::report::list::ListReasonsBuilder;
use crate::HttpClientRef;

/// Report endpoint handler builder.
#[derive(Clone, Debug)]
pub struct ReportBuilder {
    http_client: HttpClientRef,
}

impl ReportBuilder {
    pub fn new(http_client: HttpClientRef) -> Self {
        Self { http_client }
    }

    /// Get a list of report reasons.
    ///
    /// <https://api.mangadex.org/docs.html#operation/get-report-reasons-by-category>
    pub fn list(&self) -> ListReasonsBuilder {
        ListReasonsBuilder::default().http_client(self.http_client.clone())
    }

    /// Create a new report.
    ///
    /// <https://api.mangadex.org/docs.html#operation/post-report>
    pub fn create(&self) -> CreateReportBuilder {
        CreateReportBuilder::default().http_client(self.http_client.clone())
    }
}
