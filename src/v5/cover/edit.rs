//! Builder for the edit cover endpoint.
//!
//! <https://api.mangadex.org/docs.html#operation/edit-cover>
//!
//! # Examples
//!
//! ```rust
//! use uuid::Uuid;
//!
//! use mangadex_api::v5::MangaDexClient;
//! use mangadex_api::types::{Password, Username};
//!
//! # async fn run() -> anyhow::Result<()> {
//! let client = MangaDexClient::default();
//!
//! let _login_res = client
//!     .auth()
//!     .login()
//!     .username(Username::parse("myusername")?)
//!     .password(Password::parse("hunter23")?)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! let cover_id = Uuid::new_v4();
//! let res = client
//!     .cover()
//!     .edit()
//!     .cover_id(&cover_id)
//!     .volume("1")
//!     .version(2u32)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! println!("edit: {:?}", res);
//! # Ok(())
//! # }
//! ```

use derive_builder::Builder;
use serde::Serialize;
use uuid::Uuid;

use crate::v5::schema::CoverResponse;
use crate::HttpClientRef;

#[derive(Debug, Serialize, Clone, Builder)]
#[serde(rename_all = "camelCase")]
#[builder(setter(into, strip_option), pattern = "owned")]
#[non_exhaustive]
pub struct EditCover<'a> {
    /// This should never be set manually as this is only for internal use.
    #[serde(skip)]
    #[builder(pattern = "immutable")]
    pub http_client: HttpClientRef,

    #[serde(skip)]
    pub cover_id: &'a Uuid,

    #[builder(default)]
    pub volume: &'a str,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub description: Option<&'a str>,

    pub version: u32,
}

endpoint! {
    PUT ("/cover/{}", cover_id),
    #[body auth] EditCover<'_>,
    #[flatten_result] CoverResponse
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "chrono")]
    use chrono::{DateTime, Utc};
    #[cfg(not(feature = "time"))]
    use fake::faker::chrono::en::DateTime;
    use fake::faker::lorem::en::Sentence;
    use fake::Fake;
    use serde_json::json;
    #[cfg(feature = "time")]
    use time::OffsetDateTime;
    use url::Url;
    use uuid::Uuid;
    use wiremock::matchers::{header, method, path_regex};
    use wiremock::{Mock, MockServer, ResponseTemplate};

    use crate::types::MangaDexDateTime;
    use crate::v5::AuthTokens;
    use crate::{HttpClient, MangaDexClient};

    #[tokio::test]
    async fn edit_cover_fires_a_request_to_base_url() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .auth_tokens(AuthTokens {
                session: "sessiontoken".to_string(),
                refresh: "refreshtoken".to_string(),
            })
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let cover_id = Uuid::new_v4();
        let description: String = Sentence(1..3).fake();

        let datetime = {
            #[cfg(not(any(feature = "chrono", feature = "time")))]
            let datetime: String = DateTime().fake();
            #[cfg(feature = "chrono")]
            let datetime: DateTime<Utc> = DateTime().fake();
            #[cfg(feature = "time")]
            let datetime = OffsetDateTime::now_utc();

            MangaDexDateTime::new(&datetime)
        };

        let _expected_body = json!({
            "volume": "1",
            "version": 2
        });
        let response_body = json!({
            "result": "ok",
            "response": "entity",
            "data": {
                "id": cover_id,
                "type": "cover_art",
                "attributes": {
                    "volume": "1",
                    "fileName": "1.jpg",
                    "description": description,
                    "version": 1,
                    "createdAt": datetime.to_string(),
                    "updatedAt": datetime.to_string(),

                },
                "relationships": []
            }
        });

        Mock::given(method("PUT"))
            .and(path_regex(r"/cover/[0-9a-fA-F-]+"))
            .and(header("Authorization", "Bearer sessiontoken"))
            .and(header("Content-Type", "application/json"))
            // TODO: Make the request body check work.
            // .and(body_json(expected_body))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let _ = mangadex_client
            .cover()
            .edit()
            .cover_id(&cover_id)
            .volume("1")
            .version(2u32)
            .build()?
            .send()
            .await?;

        Ok(())
    }
}
