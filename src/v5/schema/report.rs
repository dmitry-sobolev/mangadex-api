use serde::Deserialize;

use crate::types::ReportCategory;
use crate::v5::schema::LocalizedString;

/// Report reason response object.
#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ReportReasonAttributes {
    pub reason: LocalizedString,
    pub details_required: bool,
    pub category: ReportCategory,
    pub version: u32,
}
