use serde::Deserialize;
use uuid::Uuid;

#[derive(Debug, thiserror::Error, Deserialize)]
#[error("Bad request")]
pub struct MangaDexErrorResponse {
    #[serde(default)]
    pub errors: Vec<MangaDexError>,
}

#[derive(Debug, thiserror::Error, PartialEq, Eq, Deserialize, Clone)]
#[error("API error")]
pub struct MangaDexError {
    pub id: Uuid,
    /// HTTP status code.
    pub status: u16,
    /// Error title.
    pub title: Option<String>,
    /// Description about the error.
    pub detail: Option<String>,
}
