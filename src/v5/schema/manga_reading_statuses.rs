use std::collections::HashMap;

use serde::Deserialize;
use uuid::Uuid;

use crate::types::ReadingStatus;

/// Reading statuses for followed manga.
#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct MangaReadingStatuses {
    /// Mapping of manga ID to reading status.
    pub statuses: HashMap<Uuid, ReadingStatus>,
}
