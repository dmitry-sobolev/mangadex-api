use std::collections::HashMap;

use serde::Deserialize;
use uuid::Uuid;

/// Response struct for the manga aggregate endpoint (GET `/manga/:id/aggregate`).
#[derive(Clone, Debug, Default, Deserialize, PartialEq)]
#[non_exhaustive]
pub struct MangaAggregate {
    /// Object with (volume_number, volume) key-value pairs.
    pub volumes: HashMap<String, VolumeAggregate>,
}

#[derive(Clone, Debug, Default, Deserialize, PartialEq)]
#[non_exhaustive]
pub struct VolumeAggregate {
    /// Volume number.
    pub volume: String,
    /// Number of chapter translations for the volume.
    pub count: u32,
    /// Object with (chapter_number, chapter) key-value pairs.
    pub chapters: HashMap<String, ChapterAggregate>,
}

#[derive(Clone, Debug, Default, Deserialize, PartialEq)]
#[non_exhaustive]
pub struct ChapterAggregate {
    /// Chapter number.
    pub chapter: String,
    pub id: Uuid,
    /// Number of translations for the chapter.
    pub count: u32,
}
