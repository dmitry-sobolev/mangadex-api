use derive_builder::Builder;
use serde::Serialize;
use uuid::Uuid;

use crate::types::{ContentRating, Demographic, Language, MangaStatus, Tag};
use crate::v5::schema::{LocalizedString, MangaLinks};

// `derive_builder` doesn't support generating getters via flattening.
#[allow(unused)]
#[derive(Debug, Builder, Default, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct MangaRequest {
    #[builder(setter(each = "add_title"))]
    pub title: LocalizedString,
    #[builder(default)]
    pub alt_titles: Option<Vec<LocalizedString>>,
    #[builder(default)]
    pub description: Option<LocalizedString>,
    #[builder(default)]
    pub authors: Option<Vec<Uuid>>,
    #[builder(default)]
    pub artists: Option<Vec<Uuid>>,
    #[builder(default)]
    pub links: Option<MangaLinks>,
    #[builder(default)]
    pub original_language: Option<Language>,
    #[builder(default)]
    pub last_volume: Option<String>,
    #[builder(default)]
    pub last_chapter: Option<String>,
    #[builder(default)]
    pub publication_demographic: Option<Demographic>,
    #[builder(default)]
    pub status: Option<MangaStatus>,
    /// Year the manga was released.
    #[builder(default)]
    pub year: Option<u16>,
    #[builder(default)]
    pub content_rating: Option<ContentRating>,
    #[builder(default)]
    pub tags: Option<Vec<Tag>>,
    #[builder(default)]
    pub primary_cover: Option<Uuid>,
    pub version: u32,
}
