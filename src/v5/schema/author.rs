use serde::Deserialize;
use url::Url;

use crate::types::MangaDexDateTime;
use crate::v5::schema::{localizedstring_array_or_map, LocalizedString};

/// General author information.
#[derive(Clone, Debug, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
#[non_exhaustive]
pub struct AuthorAttributes {
    pub name: String,
    pub image_url: Option<String>,
    #[serde(with = "localizedstring_array_or_map")]
    pub biography: LocalizedString,
    pub twitter: Option<Url>,
    pub pixiv: Option<Url>,
    pub melon_book: Option<Url>,
    pub fan_box: Option<Url>,
    pub booth: Option<Url>,
    pub nico_video: Option<Url>,
    pub skeb: Option<Url>,
    pub fantia: Option<Url>,
    pub tumblr: Option<Url>,
    pub youtube: Option<Url>,
    pub website: Option<Url>,
    pub version: u32,
    /// Datetime in `YYYY-MM-DDTHH:MM:SS+HH:MM` format.
    pub created_at: MangaDexDateTime,
    /// Datetime in `YYYY-MM-DDTHH:MM:SS+HH:MM` format.
    pub updated_at: Option<MangaDexDateTime>,
}
