use serde::Deserialize;

use crate::types::{
    ContentRating, Demographic, Language, MangaDexDateTime, MangaState, MangaStatus,
};
use crate::v5::schema::{ApiObject, LocalizedString, MangaLinks, TagAttributes};

/// General manga information.
#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
#[non_exhaustive]
pub struct MangaAttributes {
    pub title: LocalizedString,
    pub alt_titles: Vec<LocalizedString>,
    pub description: LocalizedString,
    // This field isn't always returned, so default to `false` when it isn't.
    // The decision to use the default value is to maintain compatibility if the MangaDex API
    // fixes this by always returning this field.
    #[serde(default)]
    pub is_locked: bool,
    pub links: Option<MangaLinks>,
    pub original_language: Language,
    pub last_volume: Option<String>,
    pub last_chapter: Option<String>,
    pub publication_demographic: Option<Demographic>,
    pub status: MangaStatus,
    pub year: Option<u16>,
    pub content_rating: Option<ContentRating>,
    pub tags: Vec<ApiObject<TagAttributes>>,
    /// The staff approval status of the manga.
    ///
    /// When a new manga is created with the Manga Create endpoint, it is in a "draft" state.
    /// When it is submitted (committed), it must be approved (published) or rejected by staff.
    ///
    /// Manga that is in the "draft" state is not available through the search,
    /// however, endpoints to list or retrieve the Manga Drafts are available.
    pub state: MangaState,
    /// Datetime in `YYYY-MM-DDTHH:MM:SS+HH:MM` format.
    pub created_at: MangaDexDateTime,
    /// Datetime in `YYYY-MM-DDTHH:MM:SS+HH:MM` format.
    pub updated_at: Option<MangaDexDateTime>,
    pub version: u32,
}
