use serde::Deserialize;
use url::Url;

use crate::types::{Language, MangaDexDateTime};
use crate::v5::schema::{ApiObject, LocalizedString, UserAttributes};

/// General scanlation group information.
#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[non_exhaustive]
pub struct ScanlationGroupAttributes {
    pub name: String,
    pub alt_names: Vec<LocalizedString>,
    // This is only returned from the list endpoint.
    pub leader: Option<ApiObject<UserAttributes>>,
    // This is only returned from the list endpoint.
    pub members: Option<Vec<ApiObject<UserAttributes>>>,
    pub website: Option<String>,
    pub irc_server: Option<String>,
    pub irc_channel: Option<String>,
    pub discord: Option<String>,
    pub contact_email: Option<String>,
    pub description: Option<String>,
    pub twitter: Option<Url>,
    /// Languages the scanlation primarily translates or uploads works into.
    pub focused_language: Option<Vec<Language>>,
    pub locked: bool,
    pub official: bool,
    pub verified: bool,
    pub version: u32,
    /// Datetime in `YYYY-MM-DDTHH:MM:SS+HH:MM` format.
    pub created_at: MangaDexDateTime,
    /// Datetime in `YYYY-MM-DDTHH:MM:SS+HH:MM` format.
    pub updated_at: MangaDexDateTime,
}
