use serde::Deserialize;

use crate::types::ReadingStatus;

/// Reading status for a single manga.
#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct MangaReadingStatus {
    pub status: ReadingStatus,
}
