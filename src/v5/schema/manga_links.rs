use serde::Deserialize;

/// Related links for a manga.
#[derive(Clone, Debug, Deserialize, Hash, PartialEq)]
#[serde(rename_all = "snake_case")]
#[non_exhaustive]
pub struct MangaLinks {
    /// AniList ID
    #[serde(rename = "al")]
    pub anilist: Option<String>,
    /// Anime-Planet slug
    #[serde(rename = "ap")]
    pub anime_planet: Option<String>,
    /// BookWalker URI
    #[serde(rename = "bw")]
    pub book_walker: Option<BookWalker>,
    /// Kitsu ID
    #[serde(rename = "kt")]
    pub kitsu: Option<String>,
    /// MangaUpdates ID
    #[serde(rename = "mu")]
    pub manga_updates: Option<MangaUpdates>,
    /// NovelUpdates slug
    #[serde(rename = "nu")]
    pub novel_updates: Option<NovelUpdates>,
    /// Amazon Product URL
    #[serde(rename = "amz")]
    pub amazon: Option<String>,
    /// CDJapan URL
    #[serde(rename = "cdj")]
    pub cd_japan: Option<String>,
    /// EbookJapan URL
    #[serde(rename = "ebj")]
    pub ebook_japan: Option<String>,
    /// MyAnimeList ID
    #[serde(rename = "mal")]
    pub my_anime_list: Option<MyAnimeList>,
    /// Raw URL
    pub raw: Option<String>,
    /// Official English URL
    #[serde(rename = "engtl")]
    pub english_translation: Option<String>,
    // TODO: Known issue: Manga ID "f9c33607-9180-4ba6-b85c-e4b5faee7192" has an unknown key of "dj".
    #[serde(rename = "dj", skip)]
    pub unknown: Option<String>,
}

/// BookWalker URI.
///
/// Example: "`series/91701`".
#[derive(Clone, Debug, Deserialize, Hash, PartialEq)]
pub struct BookWalker(pub String);

impl std::fmt::Display for BookWalker {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        fmt.write_str(&*format!("https://bookwalker.jp/{}", self.0))
    }
}

/// MangaUpdates ID.
///
/// Example: "`132515`".
#[derive(Clone, Debug, Deserialize, Hash, PartialEq)]
pub struct MangaUpdates(pub String);

impl std::fmt::Display for MangaUpdates {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        fmt.write_str(&*format!(
            "https://www.mangaupdates.com/series.html?id={}",
            self.0
        ))
    }
}

/// MyAnimeList ID.
///
/// Example: "`98436`".
#[derive(Clone, Debug, Deserialize, Hash, PartialEq)]
pub struct MyAnimeList(pub String);

impl std::fmt::Display for MyAnimeList {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        fmt.write_str(&*format!("https://myanimelist.net/manga/{}", self.0))
    }
}

/// NovelUpdates slug.
///
/// Example: "`novel-updates`".
#[derive(Clone, Debug, Deserialize, Hash, PartialEq)]
pub struct NovelUpdates(pub String);

impl std::fmt::Display for NovelUpdates {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        fmt.write_str(&*format!("https://www.novelupdates.com/series/{}/", self.0))
    }
}
