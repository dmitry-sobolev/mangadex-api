//! Builder for the chapter list endpoint.
//!
//! <https://api.mangadex.org/docs.html#operation/get-chapter>
//!
//! # Examples
//!
//! ```rust
//! use mangadex_api::v5::MangaDexClient;
//!
//! # async fn run() -> anyhow::Result<()> {
//! let client = MangaDexClient::default();
//!
//! let chapter_res = client
//!     .chapter()
//!     .list()
//!     .title("summoning")
//!     .build()?
//!     .send()
//!     .await?;
//!
//! println!("chapters: {:?}", chapter_res);
//! # Ok(())
//! # }
//! ```

use derive_builder::Builder;
use serde::Serialize;
use uuid::Uuid;

use crate::types::{
    ChapterSortOrder, ContentRating, IncludeFutureUpdates, Language, MangaDexDateTime,
    ReferenceExpansionResource,
};
use crate::v5::schema::ChapterListResponse;
use crate::HttpClientRef;

#[derive(Debug, Serialize, Clone, Builder, Default)]
#[serde(rename_all = "camelCase")]
#[builder(setter(into, strip_option), default, pattern = "owned")]
#[non_exhaustive]
pub struct ListChapter<'a> {
    /// This should never be set manually as this is only for internal use.
    #[serde(skip)]
    #[builder(pattern = "immutable")]
    pub http_client: HttpClientRef,

    pub limit: Option<u32>,
    pub offset: Option<u32>,
    #[serde(rename = "ids")]
    #[builder(setter(each = "add_chapter_id"))]
    pub chapter_ids: Vec<&'a Uuid>,
    pub title: Option<String>,
    #[builder(setter(each = "add_group"))]
    pub groups: Vec<&'a Uuid>,
    pub uploader: Option<&'a Uuid>,
    #[serde(rename = "manga")]
    pub manga_id: Option<&'a Uuid>,
    #[serde(rename = "volume")]
    #[builder(setter(each = "add_volume"))]
    pub volumes: Vec<String>,
    /// Chapter number in the series or volume.
    #[builder(setter(each = "add_chapter"))]
    pub chapters: Vec<String>,
    #[serde(rename = "translatedLanguage")]
    #[builder(setter(each = "add_translated_language"))]
    pub translated_languages: Vec<Language>,
    #[serde(rename = "originalLanguage")]
    #[builder(setter(each = "add_original_language"))]
    pub original_languages: Vec<Language>,
    #[serde(rename = "excludedOriginalLanguage")]
    #[builder(setter(each = "exclude_original_language"))]
    pub excluded_original_languages: Vec<Language>,
    #[builder(setter(each = "add_content_rating"))]
    pub content_rating: Vec<ContentRating>,
    /// Flag to include future chapter updates in the results.
    ///
    /// Default: `IncludeFutureUpdates::Include` (1)
    pub include_future_updates: Option<IncludeFutureUpdates>,
    /// DateTime string with following format: `YYYY-MM-DDTHH:MM:SS`.
    pub created_at_since: Option<MangaDexDateTime>,
    /// DateTime string with following format: `YYYY-MM-DDTHH:MM:SS`.
    pub updated_at_since: Option<MangaDexDateTime>,
    /// DateTime string with following format: `YYYY-MM-DDTHH:MM:SS`.
    pub publish_at_since: Option<MangaDexDateTime>,
    pub order: Option<ChapterSortOrder>,
    #[builder(setter(each = "include"))]
    pub includes: Vec<ReferenceExpansionResource>,
}

endpoint! {
    GET "/chapter",
    #[query] ListChapter<'_>,
    #[flatten_result] ChapterListResponse
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "chrono")]
    use chrono::{DateTime, Utc};
    #[cfg(not(feature = "time"))]
    use fake::faker::chrono::en::DateTime;
    use fake::faker::internet::en::Password;
    use fake::faker::name::en::Name;
    use fake::Fake;
    #[cfg(feature = "chrono")]
    use regex::Regex;
    use serde_json::json;
    #[cfg(feature = "time")]
    use time::OffsetDateTime;
    use url::Url;
    use uuid::Uuid;
    use wiremock::matchers::{method, path};
    #[cfg(feature = "chrono")]
    use wiremock::Request;
    use wiremock::{Mock, MockServer, ResponseTemplate};

    use crate::error::Error;
    use crate::types::{Language, MangaDexDateTime, ResponseType};
    use crate::{HttpClient, MangaDexClient};

    #[tokio::test]
    async fn list_chapter_fires_a_request_to_base_url() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let chapter_id = Uuid::new_v4();
        let uploader_id = Uuid::new_v4();
        let chapter_title: String = Name().fake();
        let hash: String = Password(16..24).fake();

        let datetime = {
            #[cfg(not(any(feature = "chrono", feature = "time")))]
            let datetime: String = DateTime().fake();
            #[cfg(feature = "chrono")]
            let datetime: DateTime<Utc> = DateTime().fake();
            #[cfg(feature = "time")]
            let datetime = OffsetDateTime::now_utc();

            MangaDexDateTime::new(&datetime)
        };

        let response_body = json!({
            "result": "ok",
            "response": "collection",
            "data": [
                {
                    "id": chapter_id,
                    "type": "chapter",
                    "attributes": {
                        "title": chapter_title,
                        "volume": "1",
                        "chapter": "1.5",
                        "translatedLanguage": "en",
                        "hash": hash,
                        "data": [
                            "1.jpg"
                        ],
                        "dataSaver": [
                            "1.jpg"
                        ],
                        "uploader": uploader_id,
                        "version": 1,
                        "createdAt": datetime.to_string(),
                        "updatedAt": datetime.to_string(),
                        "publishAt": datetime.to_string(),
                    },
                    "relationships": []
                }
            ],
            "limit": 1,
            "offset": 0,
            "total": 1
        });

        Mock::given(method("GET"))
            .and(path("/chapter"))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let res = mangadex_client
            .chapter()
            .search()
            .limit(1u32)
            .build()?
            .send()
            .await?;

        assert_eq!(res.response, ResponseType::Collection);
        let chapter = &res.data[0];
        assert_eq!(chapter.id, chapter_id);
        assert_eq!(chapter.attributes.title, chapter_title);
        assert_eq!(chapter.attributes.volume, Some("1".to_string()));
        assert_eq!(chapter.attributes.chapter, Some("1.5".to_string()));
        assert_eq!(chapter.attributes.translated_language, Language::English);
        assert_eq!(chapter.attributes.hash, hash);
        assert_eq!(chapter.attributes.data, vec!["1.jpg"]);
        assert_eq!(chapter.attributes.data_saver, vec!["1.jpg"]);
        assert_eq!(chapter.attributes.uploader, Some(uploader_id));
        assert_eq!(chapter.attributes.version, 1);
        assert_eq!(
            chapter.attributes.created_at.to_string(),
            datetime.to_string()
        );
        assert_eq!(
            chapter.attributes.updated_at.as_ref().unwrap().to_string(),
            datetime.to_string()
        );
        assert_eq!(
            chapter.attributes.publish_at.to_string(),
            datetime.to_string()
        );

        Ok(())
    }

    #[tokio::test]
    async fn list_chapter_handles_400() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client: HttpClient = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let error_id = Uuid::new_v4();

        let response_body = json!({
            "result": "error",
            "errors": [{
                "id": error_id.to_string(),
                "status": 400,
                "title": "Invalid limit",
                "detail": "Limit must be between 1 and 100"
            }]
        });

        Mock::given(method("GET"))
            .and(path("/chapter"))
            .respond_with(ResponseTemplate::new(400).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let res = mangadex_client
            .chapter()
            .search()
            .limit(0u32)
            .build()?
            .send()
            .await
            .expect_err("expected error");

        if let Error::Api(errors) = res {
            assert_eq!(errors.errors.len(), 1);

            assert_eq!(errors.errors[0].id, error_id);
            assert_eq!(errors.errors[0].status, 400);
            assert_eq!(errors.errors[0].title, Some("Invalid limit".to_string()));
            assert_eq!(
                errors.errors[0].detail,
                Some("Limit must be between 1 and 100".to_string())
            );
        }

        Ok(())
    }

    #[cfg(feature = "chrono")]
    async fn list_chapter_datetime_setup(client: &MangaDexClient) {
        let date_time: DateTime<Utc> = DateTime().fake();
        let _ = client
            .chapter()
            .search()
            .created_at_since(date_time)
            .updated_at_since(date_time)
            .publish_at_since(date_time)
            .build()
            .unwrap()
            .send()
            .await;
    }

    #[cfg(feature = "chrono")]
    crate::test_serialize_mangadex_datetime!(
        #[query_string]
        ["createdAtSince", "updatedAtSince", "publishAtSince"],
        list_chapter_datetime_setup
    );
}
