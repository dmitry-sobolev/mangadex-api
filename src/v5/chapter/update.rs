//! Builder for the chapter update endpoint.
//!
//! <https://api.mangadex.org/docs.html#operation/put-chapter-id>
//!
//! # Examples
//!
//! ```rust
//! use uuid::Uuid;
//!
//! use mangadex_api::v5::MangaDexClient;
//! use mangadex_api::types::{Password, Username};
//!
//! # async fn run() -> anyhow::Result<()> {
//! let client = MangaDexClient::default();
//!
//! let _login_res = client
//!     .auth()
//!     .login()
//!     .username(Username::parse("myusername")?)
//!     .password(Password::parse("hunter23")?)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! let chapter_id = Uuid::new_v4();
//! let res = client
//!     .chapter()
//!     .update()
//!     .chapter_id(&chapter_id)
//!     .title("Updated Chapter Title")
//!     .version(2u32)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! println!("update: {:?}", res);
//! # Ok(())
//! # }
//! ```

use derive_builder::Builder;
use serde::Serialize;
use uuid::Uuid;

use crate::types::Language;
use crate::v5::schema::ChapterResponse;
use crate::HttpClientRef;

#[derive(Debug, Serialize, Clone, Builder)]
#[serde(rename_all = "camelCase")]
#[builder(setter(into, strip_option), pattern = "owned")]
#[non_exhaustive]
pub struct UpdateChapter<'a> {
    /// This should never be set manually as this is only for internal use.
    #[serde(skip)]
    #[builder(pattern = "immutable")]
    pub http_client: HttpClientRef,

    #[serde(skip)]
    pub chapter_id: &'a Uuid,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub title: Option<&'a str>,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub volume: Option<&'a str>,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub chapter: Option<&'a str>,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub translated_language: Option<Language>,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub groups: Option<Vec<Uuid>>,

    pub version: u32,
}

endpoint! {
    PUT ("/chapter/{}", chapter_id),
    #[body auth] UpdateChapter<'_>,
    #[flatten_result] ChapterResponse
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "chrono")]
    use chrono::{DateTime, Utc};
    #[cfg(not(feature = "time"))]
    use fake::faker::chrono::en::DateTime;
    use fake::faker::internet::en::Password;
    use fake::faker::name::en::Name;
    use fake::Fake;
    use serde_json::json;
    #[cfg(feature = "time")]
    use time::OffsetDateTime;
    use url::Url;
    use uuid::Uuid;
    use wiremock::matchers::{body_json, header, method, path_regex};
    use wiremock::{Mock, MockServer, ResponseTemplate};

    use crate::types::MangaDexDateTime;
    use crate::v5::AuthTokens;
    use crate::{HttpClient, MangaDexClient};

    #[tokio::test]
    async fn update_chapter_fires_a_request_to_base_url() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .auth_tokens(AuthTokens {
                session: "sessiontoken".to_string(),
                refresh: "refreshtoken".to_string(),
            })
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let chapter_id = Uuid::new_v4();
        let uploader_id = Uuid::new_v4();
        let chapter_title: String = Name().fake();
        let hash: String = Password(16..24).fake();

        let datetime = {
            #[cfg(not(any(feature = "chrono", feature = "time")))]
            let datetime: String = DateTime().fake();
            #[cfg(feature = "chrono")]
            let datetime: DateTime<Utc> = DateTime().fake();
            #[cfg(feature = "time")]
            let datetime = OffsetDateTime::now_utc();

            MangaDexDateTime::new(&datetime)
        };

        let expected_body = json!({
            "version": 2
        });
        let response_body = json!({
            "result": "ok",
            "response": "entity",
            "data": {
                "id": chapter_id,
                "type": "chapter",
                "attributes": {
                    "title": chapter_title,
                    "volume": "1",
                    "chapter": "1.5",
                    "translatedLanguage": "en",
                    "hash": hash,
                    "data": [
                        "1.jpg"
                    ],
                    "dataSaver": [
                        "1.jpg"
                    ],
                    "uploader": uploader_id,
                    "version": 1,
                    "createdAt": datetime.to_string(),
                    "updatedAt": datetime.to_string(),

                    "publishAt": datetime.to_string(),
                },
                "relationships": []
            }
        });

        Mock::given(method("PUT"))
            .and(path_regex(r"/chapter/[0-9a-fA-F-]+"))
            .and(header("Authorization", "Bearer sessiontoken"))
            .and(header("Content-Type", "application/json"))
            .and(body_json(expected_body))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let _ = mangadex_client
            .chapter()
            .update()
            .chapter_id(&chapter_id)
            .version(2u32)
            .build()?
            .send()
            .await?;

        Ok(())
    }
}
