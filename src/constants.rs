pub const API_URL: &str = "https://api.mangadex.org";
/// URL for downloading media such as cover images.
pub const CDN_URL: &str = "https://uploads.mangadex.org";

#[cfg(feature = "chrono")]
pub(crate) const MANGADEX_DATETIME_DE_FORMAT: &str = "%Y-%m-%dT%H:%M:%S%:z";
#[cfg(feature = "time")]
pub(crate) const MANGADEX_DATETIME_DE_FORMAT: &str =
    "[year]-[month]-[day]T[hour]:[minute]:[second][offset_hour sign:mandatory]:[offset_minute]";

#[cfg(feature = "chrono")]
pub(crate) const MANGADEX_DATETIME_SER_FORMAT: &str = "%Y-%m-%dT%H:%M:%S";
#[cfg(feature = "time")]
pub(crate) const MANGADEX_DATETIME_SER_FORMAT: &str =
    "[year]-[month]-[day]T[hour]:[minute]:[second]";
