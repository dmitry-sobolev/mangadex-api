# mangadex_api

The `mangadex-api` crate provides a convenient, high-level wrapper
[client][library-client] for the [MangaDex API][mangadex-api-url],
written in [Rust][rust-homepage].

It covers all public endpoints covered by [their documentation][mangadex-api-url].

[Documentation (docs.rs)](https://docs.rs/mangadex_api)

[Documentation (Project `main` branch)](https://gondolyr.gitlab.io/mangadex-api/mangadex_api)

Please note that as MangaDex is still in beta, this SDK will be subject to sudden breaking changes.

## Disclaimer

`mangadex-api` is not affiliated with [MangaDex][mangadex-homepage].

# Table of Contents

* [Requirements][readme-section-requirements]
* [How to install][readme-section-install]
* [Features][readme-section-features]
* [HTTP Client][readme-section-http-client]
* [Getting Started][readme-section-getting-started]
* [Using a custom reqwest Client][readme-section-custom-client]
* [Searching manga by title][readme-section-searching-manga]
* [Downloading chapter pages][readme-section-downloading-chapter-pages]
* [Downloading a manga's main cover image][readme-section-downloading-manga-main-cover]
* [Changelog][readme-section-changelog]
* [License][readme-section-license]
    * [Contribution terms and conditions][readme-section-contribution]
* [Contributing][readme-section-contributing]

# Requirements

[Back to top][readme-section-toc]

* [Rust 1.56+][rust-homepage]

# How to install

[Back to top][readme-section-toc]

Add `mangadex-api` to your dependencies:

```toml
[dependencies]
# ...
mangadex-api = "1.0.0-alpha.9"
```

If you are using [`cargo-edit`](https://github.com/killercup/cargo-edit), run

```bash
cargo add mangadex-api
```

# Features

[Back to top][readme-section-toc]

All features are not included by default. To enable them, add any of the following to your project's `Cargo.toml` file.

- `chrono`

  Enable the use of the [`chrono` library](https://docs.rs/chrono) for fields that contain datetime objects.

  This **cannot** be used with the `time` feature.

- `multi-thread`

  Enable the `MangaDexClient` to be thread-safe, at the cost of operations being slightly more expensive.

- `time`

  Enable the use of the [`time` library](https://docs.rs/time) for fields that contain datetime objects.

  This **cannot** be used with the `chrono` feature.

- `upload`

  Enable endpoints that upload files to MangaDex.

For example, to enable the `time` feature, add the following to your `Cargo.toml` file:

```toml
mangadex-api = { version = "1.0.0-alpha.9", features = ["time"] }
```

# HTTP Client

[Back to top][readme-section-toc]

The [`mangadex_api::MangaDexClient`][library-client] is asynchronous, using
[`reqwest`][reqwest] as the HTTP client.

# Getting Started

[Back to top][readme-section-toc]

This example demonstrates how to fetch a random manga.

```rust
use mangadex_api::v5::MangaDexClient;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let client = MangaDexClient::default();

    let random_manga = client
        .manga()
        .random()
        .build()?
        .send()
        .await?;

    println!("{:?}", random_manga);

    Ok(())
}
```

# Using a custom reqwest Client

[Back to top][readme-section-toc]

By default, [`mangadex_api::MangaDexClient`][library-client] will use the default
[`reqwest::Client`][reqwest-client] settings.

You may provide your own [`reqwest::Client`][reqwest-client] to customize options such as the
request timeout.

```rust
use reqwest::Client;

use mangadex_api::v5::MangaDexClient;

# async fn run() -> anyhow::Result<()> {
let reqwest_client = Client::builder()
    .timeout(std::time::Duration::from_secs(10))
    .build()?;

let client = MangaDexClient::new(reqwest_client);
# Ok(())
# }
```

# Searching manga by title

[Back to top][readme-section-toc]

Reference: <https://api.mangadex.org/docs.html#operation/get-search-manga>

```rust
use mangadex_api::v5::MangaDexClient;

# async fn run() -> anyhow::Result<()> {
let client = MangaDexClient::default();

let manga_results = client
    .manga()
    .search()
    .title("full metal")
    .build()?
    .send()
    .await?;

println!("manga results = {:?}", manga_results);
# Ok(())
# }
```

# Searching manga by title with reference expansion

[Back to top][readme-section-toc]

Every fetch will include all relationships but with minimal information such as the relationship type and ID. Reference expansion will include the full JSON object in the results for the types that are added to the request.

In the example below, any associated authors in the list of relationships will provide detailed information such as the author's name, biography, and website in the results.

References:

- <https://api.mangadex.org/docs.html#section/Reference-Expansion>
- Endpoint: <https://api.mangadex.org/docs.html#operation/get-search-manga>
- Author object: <https://api.mangadex.org/docs.html#operation/get-author-id>

```rust
use mangadex_api::types::RelationshipType;
use mangadex_api::v5::schema::RelatedAttributes;
use mangadex_api::v5::MangaDexClient;

# async fn run() -> anyhow::Result<()> {
let client = MangaDexClient::default();

let manga_results = client
    .manga()
    .search()
    .title("full metal")
    .include(&RelationshipType::Author)
    .build()?
    .send()
    .await?;

println!("manga results = {:?}", manga_results);

let authors = manga_results.data.iter().filter_map(|manga| {
    for rel in &manga.relationships {
        if rel.type_ == RelationshipType::Author {
            return Some(rel);
        }
    }

    None
});

for author in authors {
    if let Some(RelatedAttributes::Author(author_attributes)) = &author.attributes {
        println!("{} - {}", author.id, author_attributes.name);
    }
}
# Ok(())
# }
```

# Downloading chapter pages

[Back to top][readme-section-toc]

Reference: <https://api.mangadex.org/docs.html#section/Reading-a-chapter-using-the-API>

```rust
// Imports used for downloading the pages to a file.
// They are not used because we're just printing the raw bytes.
// use std::fs::File;
// use std::io::Write;

use reqwest::Url;
use uuid::Uuid;

use mangadex_api::v5::MangaDexClient;

# async fn run() -> anyhow::Result<()> {
let client = MangaDexClient::default();

let chapter_id = Uuid::new_v4();
let chapter = client
    .chapter()
    .get()
    .chapter_id(&chapter_id)
    .build()?
    .send()
    .await?;

let at_home_url = client
    .at_home()
    .server()
    .chapter_id(&chapter_id)
    .build()?
    .send()
    .await?
    .base_url;

let http_client = reqwest::Client::new();

// Original quality. Use `.data.attributes.data_saver` for smaller, compressed images.
let page_filenames = chapter.data.attributes.data;
for filename in page_filenames {
    // If using the data-saver option, use "/data-saver/" instead of "/data/" in the URL.
    let page_url = at_home_url
        .join(&format!(
            "/{quality_mode}/{chapter_hash}/{page_filename}",
            quality_mode = "data",
            chapter_hash = chapter.data.attributes.hash,
            page_filename = filename
        ))
        .unwrap();

    let res = http_client.get(page_url).send().await?;
    // The data should be streamed rather than downloading the data all at once.
    let bytes = res.bytes().await?;

    // This is where you would download the file but for this example,
    // we're just printing the raw data.
    // let mut file = File::create(&filename)?;
    // let _ = file.write_all(&bytes);
    println!("Chunk: {:?}", bytes);
}

# Ok(())
# }
```

# Downloading a manga's main cover image

[Back to top][readme-section-toc]

While this example could directly get the cover information by passing in the cover ID,
it is not often that one would have the ID off-hand, so the most common method would be from a
manga result.

If you want to get all of a manga's cover images, you will need to use the [cover list endpoint](https://api.mangadex.org/docs.html#operation/get-cover)
and use the `manga[]` query parameter.

```rust
// Imports used for downloading the cover to a file.
// They are not used because we're just printing the raw bytes.
// use std::fs::File;
// use std::io::Write;

use reqwest::Url;
use uuid::Uuid;

use mangadex_api::types::RelationshipType;
use mangadex_api::v5::MangaDexClient;
use mangadex_api::CDN_URL;

# async fn run() -> anyhow::Result<()> {
let client = MangaDexClient::default();

let manga_id = Uuid::new_v4();
let manga = client
    .manga()
    .get()
    .manga_id(&manga_id)
    .build()?
    .send()
    .await?;

let cover_id = manga
    .data
    .relationships
    .iter()
    .find(|related| related.type_ == RelationshipType::CoverArt)
    .expect("no cover art found for manga")
    .id;
let cover = client
    .cover()
    .get()
    .cover_id(&cover_id)
    .build()?
    .send()
    .await?;

// This uses the best quality image.
// To use smaller, thumbnail-sized images, append any of the following:
//
// - .512.jpg
// - .256.jpg
//
// For example, "https://uploads.mangadex.org/covers/8f3e1818-a015-491d-bd81-3addc4d7d56a/4113e972-d228-4172-a885-cb30baffff97.jpg.512.jpg"
let cover_url = Url::parse(&format!(
        "{}/covers/{}/{}",
        CDN_URL, manga_id, cover.data.attributes.file_name
    ))
    .unwrap();

let http_client = reqwest::Client::new();

let res = http_client.get(cover_url).send().await?;
// The data should be streamed rather than downloading the data all at once.
let bytes = res.bytes().await?;

// This is where you would download the file but for this example, we're just printing the raw data.
// let mut file = File::create(&filename)?;
// let _ = file.write_all(&bytes);
println!("Chunk: {:?}", bytes);
# Ok(())
# }
```

# Changelog

[Back to top][readme-section-toc]

The changelog can be found [here][changelog].

Changes are added manually to keep the changelog human-readable with summaries of the changes from each version.

# License

[Back to top][readme-section-toc]

Licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE][license-apache] or <http://www.apache.org/licenses/LICENSE-2.0>)
* MIT license
  ([LICENSE-MIT][license-mit] or <http://opensource.org/licenses/MIT>)

at your option.

## Contribution

[Back to top][readme-section-toc]

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

# Contributing

[Back to top][readme-section-toc]

We welcome contributions from everyone. There are many ways to contribute and the
[CONTRIBUTING.md][contributing] document explains how you can contribute and get started.

[mangadex-api-url]: https://api.mangadex.org
[mangadex-homepage]: https://mangadex.org
[reqwest]: https://docs.rs/reqwest
[reqwest-client]: https://docs.rs/reqwest/latest/reqwest/struct.Client.html
[rust-homepage]: https://rust-lang.org

[changelog]: https://gitlab.com/gondolyr/mangadex-api/-/blob/main/CHANGELOG.md
[contributing]: https://gitlab.com/gondolyr/mangadex-api/-/blob/main/CONTRIBUTING.md
[examples-readme]: https://gitlab.com/gondolyr/mangadex-api/-/blob/main/examples/README.md
[library-client]: ./v5/struct.MangaDexClient.html
[license-apache]: https://gitlab.com/gondolyr/mangadex-api/-/blob/main/LICENSE-APACHE
[license-mit]: https://gitlab.com/gondolyr/mangadex-api/-/blob/main/LICENSE-MIT

[readme-section-changelog]: #changelog
[readme-section-contributing]: #contributing
[readme-section-contribution]: #contribution
[readme-section-custom-client]: #using-a-custom-reqwest-client
[readme-section-downloading-chapter-pages]: #downloading-chapter-pages
[readme-section-downloading-manga-main-cover]: #downloading-a-mangas-main-cover-image
[readme-section-features]: #features
[readme-section-http-client]: #http-client
[readme-section-getting-started]: #getting-started
[readme-section-install]: #how-to-install
[readme-section-license]: #license
[readme-section-requirements]: #requirements
[readme-section-running-examples]: #running-examples
[readme-section-searching-manga]: #searching-manga-by-title
[readme-requirements]: #requirements
[readme-section-toc]: #table-of-contents
