//! This example will get mappings of legacy IDs to the new UUIDs.
//!
//! # Usage
//!
//! ```
//! map_legacy_ids [OPTION] [ID...]
//! ```
//!
//! ## Options
//!
//! -h, --help  Output a usage message and exit.
//! -t, --type  Specify the type of IDs that should be mapped.
//!             Available options are:
//!                 - chapter
//!                 - group
//!                 - manga
//!                 - tag
//!
//! # Examples
//!
//! This example will return up the new UUIDs for the legacy manga IDs 18803 and 1001.
//!
//! ```
//! map_legacy_ids -t manga 18803 1001
//! ```

use structopt::StructOpt;
use uuid::Uuid;

use mangadex_api::types::LegacyMappingType;
use mangadex_api::v5::MangaDexClient;

#[derive(StructOpt)]
#[structopt(
    name = "MangaDex Legacy ID Mapping",
    about = "Get the new UUIDs from the legacy IDs"
)]
struct Opt {
    /// Space-separated list of numerical IDs.
    #[structopt()]
    ids: Vec<u64>,
    /// Legacy mapping type.
    #[structopt(short, long, default_value = "manga")]
    r#type: LegacyMappingType,
}

#[tokio::main]
async fn main() {
    let opt = Opt::from_args();

    if let Err(e) = run(opt).await {
        use std::process;
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}

/// This will get the new UUIDs from the legacy IDs.
async fn run(opt: Opt) -> anyhow::Result<()> {
    let client = MangaDexClient::default();

    let mapped_ids = client
        .legacy()
        .id_mapping()
        .map_type(opt.r#type)
        .ids(opt.ids)
        .build()?
        .send()
        .await?;

    let ids: Vec<(u64, Uuid)> = mapped_ids
        .data
        .iter()
        .map(|id_map| (id_map.attributes.legacy_id, id_map.attributes.new_id))
        .collect();

    println!("{:#?}", ids);

    Ok(())
}
