//! This example will download a chapter's pages.
//!
//! Reference: <https://api.mangadex.org/docs.html#section/Reading-a-chapter-using-the-API>
//!
//! # Usage
//!
//! ```
//! download_chapter [OPTION] [CHAPTERID]
//! ```
//!
//! ## Options
//!
//! -h, --help
//!     Output a usage message and exit.
//!
//! --data-saver
//!     Use compressed images, which have smaller filesizes.
//!
//! -o, --download
//!     Specify the directory to save the pages to.
//!
//! # Examples
//!
//! This example will get the chapter page data for the official test manga.
//!
//! ```
//! download_chapter c84f0bdd-0936-4fc3-8a7d-9b24303df33e
//! ```
//!
//! This will download the manga covers to the local filesystem at the specified directory.
//!
//! ```
//! download_chapter --download ./ c84f0bdd-0936-4fc3-8a7d-9b24303df33e
//! ```

use std::fs::{create_dir, File};
use std::io::Write;
use std::path::{Path, PathBuf};

use reqwest::Url;
use structopt::StructOpt;
use uuid::Uuid;

use mangadex_api::v5::MangaDexClient;
use mangadex_api::HttpClientRef;

#[derive(StructOpt)]
#[structopt(
    name = "Manga Chapter Downloader",
    about = "Fetch the pages for a chapter."
)]
struct Opt {
    /// Manga UUID.
    #[structopt()]
    chapter_id: Uuid,
    #[structopt(long)]
    data_saver: bool,
    /// Location to save the cover art.
    #[structopt(short, long = "download", parse(from_os_str))]
    output: Option<PathBuf>,
}

#[tokio::main]
async fn main() {
    let opt = Opt::from_args();

    if let Err(e) = run(opt).await {
        use std::process;
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}

async fn run(opt: Opt) -> anyhow::Result<()> {
    let client = MangaDexClient::default();

    if opt.output.is_some() && !opt.output.as_ref().unwrap().is_dir() {
        let _ = create_dir(&opt.output.as_ref().unwrap());
    }

    let chapter = client
        .chapter()
        .get()
        .chapter_id(&opt.chapter_id)
        .build()?
        .send()
        .await?;

    let at_home_url = client
        .at_home()
        .server()
        .chapter_id(&opt.chapter_id)
        .build()?
        .send()
        .await?
        .base_url;

    let page_filenames = if !opt.data_saver {
        chapter.data.attributes.data
    } else {
        chapter.data.attributes.data_saver
    };

    for filename in page_filenames {
        let page_url = at_home_url
            .join(&format!(
                "/{quality_mode}/{chapter_hash}/{page_filename}",
                quality_mode = if opt.data_saver { "data-saver" } else { "data" },
                chapter_hash = chapter.data.attributes.hash,
                page_filename = filename
            ))
            .unwrap();

        let page_res = reqwest::get(page_url.clone()).await?;

        if opt.output.is_some() {
            download_file(
                client.get_http_client().clone(),
                &page_url,
                opt.output.as_ref().unwrap(),
                &filename,
            )
            .await?;
        } else {
            println!("{:?} - {:#?}", filename, page_res);
        }
    }

    Ok(())
}

/// Download the URL contents into the local filesystem.
async fn download_file(
    http_client: HttpClientRef,
    url: &Url,
    output: &Path,
    file_name: &str,
) -> anyhow::Result<()> {
    #[cfg(not(feature = "multi-thread"))]
    let image_bytes = http_client
        .borrow()
        .client
        .get(url.clone())
        .send()
        .await?
        .bytes()
        .await?;
    #[cfg(feature = "multi-thread")]
    let image_bytes = http_client
        .lock()
        .await
        .client
        .get(url)
        .send()
        .await?
        .bytes()
        .await?;

    let mut file_buffer = File::create(output.join(file_name))?;
    let _ = file_buffer.write_all(&image_bytes)?;

    Ok(())
}
