# Version 1.0.0-alpha.9 (2021-11-13)

This version addresses an issue searching for manga where the `hasAvailableChapters` query parameter was always being included in the request, defaulting to `false`.

## Fixed

- ([88f4667]) Fixed an issue searching for Manga where the `hasAvailableChapters` query parameter was always included in every request, whether or not it was specified. It would default to `false` and thus filter the results.

[88f4667]: https://gitlab.com/gondolyr/mangadex-api/-/commit/88f4667

# Version 1.0.0-alpha.8 (2021-11-12)

This version addresses [issue #10](https://gitlab.com/gondolyr/mangadex-api/-/issues/10) where users are unable to access [reference expanded](https://api.mangadex.org/docs.html#section/Reference-Expansion) attributes.

## Changed

- ([a40c140]) **BREAKING** Renamed `mod_notes` field to `primary_cover` and adjusted its type from `String` to `Uuid`. This change was introduced in MangaDex 5.3.12.

## Fixed

- ([e0b5ec6]) Fixed reference expanded types from being inaccessible by making the enum `RelatedAttributes` public. This change is a quick fix to resolve [GitLab issue #10](https://gitlab.com/gondolyr/mangadex-api/-/issues/10) but the API may change to support a solution with less boilerplate code in the future.

## Internal

- ([b63135a]) Fixed an issue with how the documentation is generated on docs.rs where it would attempt to build the documentation with all features enabled. This is not possible due to `chrono` and `time` being incompatible with each other. For now, just the default list of features (currently none) will be built.

[a40c140]: https://gitlab.com/gondolyr/mangadex-api/-/commit/a40c140
[b63135a]: https://gitlab.com/gondolyr/mangadex-api/-/commit/b63135a
[e0b5ec6]: https://gitlab.com/gondolyr/mangadex-api/-/commit/e0b5ec6

# Version 1.0.0-alpha.7 (2021-11-09)

Major changes include the transition to the Rust 2021 edition and a move away from `chrono` while also providing `time` support.
`chrono` is still part of this library, however, is no longer included by default and must be enabled as a feature.

## Added

- ([7ca3b80]) Added `twitter` field to scanlation group response attributes.
- ([12b983b]) Added `twitter` field to the create scanlation group endpoint.
- ([d0d42ed]) Added `twitter` field to the update scanlation group endpoint.
- ([4dc82a2]) Added `time` as an optional feature as an alternative to `chrono` in lieu of `chrono`'s disclosed security advisories.
- ([64da46a]) Added `source` field to the upload session file response attributes.
- ([a8c8a78]) Added the Start Edit Chapter Session endpoint. This allows users to edit existing chapters such as uploading updated page images.
- ([93e7558]) Added the `hasAvailableChapters` query parameter to the Manga list endpoint.
- ([92a4313]) Added new sort order fields for the manga feed endpoints. This change adds `createdAt`, `updatedAt`, and `publishAt` as available options.
- ([cead1f2]) Added `description` field to the upload cover endpoint.

## Changed

- ([228dec9]) **BREAKING** Made `chrono` an optional feature due to the recent security advisories that have been disclosed. `chrono` uses an outdated version of `time` (0.1 at the time of writing), [which has a security notice](https://rustsec.org/advisories/RUSTSEC-2020-0071). This information is largely taken from https://passcod.name/technical/no-time-for-chrono.html.

  See the following for more information:

  RustSec:
    - [RUSTSEC-2020-0071](https://rustsec.org/advisories/RUSTSEC-2020-0071) on Time 0.1 and 0.2.7–0.2.22
    - [RUSTSEC-2020-0159](https://rustsec.org/advisories/RUSTSEC-2020-0159) on Chrono

  Mitre:
    - [CVE-2020-26235](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-26235)

  GitHub:
    - [GHSA-wcg3-cvx6-7396](https://github.com/time-rs/time/security/advisories/GHSA-wcg3-cvx6-7396)

  Chrono:
    - https://github.com/chronotope/chrono/issues/499

- ([71e26b3]) **BREAKING** Changed the default and `chrono` features to use the new `MangaDexDateTime` newtype struct. When building requests, the datetime fields should not need any special construction and users should be able to pass in the usual `DateTime` (`chrono`), `OffsetDateTime` (`time`), or `String` (default) structs and the `into()` method should be able to convert them to a `MangaDexDateTime` struct automatically. This is different from the `Username` and `Password` structs because they have built-in validation that will prevent those requests from being sent to MangaDex if they don't meet the requirements; the `MangaDexDateTime` struct is just a wrapper around the feature's respective type.

## Fixed

- [8177388] Fixed the `upload` feature so that it compiles.

## Internal

- ([dbd1256]) Update the project to use the Rust 2021 edition.
- ([7392281]) Temporarily remove `cargo-audit` from the CI/CD pipeline because of `chrono`'s security advisories. This library will continue to support `chrono` integration for the forseeable future but will not include it as a default feature.
- ([f92398f]) Consolidated the `language` module to use a macro to improve maintainability. This makes it so that new languages that are added only need to be added in a single location instead of having to copy the variant multiple times across the various function implementations.

[12b983b]: https://gitlab.com/gondolyr/mangadex-api/-/commit/12b983b
[228dec9]: https://gitlab.com/gondolyr/mangadex-api/-/commit/228dec9
[4dc82a2]: https://gitlab.com/gondolyr/mangadex-api/-/commit/4dc82a2
[64da46a]: https://gitlab.com/gondolyr/mangadex-api/-/commit/64da46a
[71e26b3]: https://gitlab.com/gondolyr/mangadex-api/-/commit/71e26b3
[7392281]: https://gitlab.com/gondolyr/mangadex-api/-/commit/7392281
[7ca3b80]: https://gitlab.com/gondolyr/mangadex-api/-/commit/7ca3b80
[8177388]: https://gitlab.com/gondolyr/mangadex-api/-/commit/8177388
[92a4313]: https://gitlab.com/gondolyr/mangadex-api/-/commit/92a4313
[93e7558]: https://gitlab.com/gondolyr/mangadex-api/-/commit/93e7558
[a8c8a78]: https://gitlab.com/gondolyr/mangadex-api/-/commit/a8c8a78
[cead1f2]: https://gitlab.com/gondolyr/mangadex-api/-/commit/cead1f2
[d0d42ed]: https://gitlab.com/gondolyr/mangadex-api/-/commit/d0d42ed
[dbd1256]: https://gitlab.com/gondolyr/mangadex-api/-/commit/dbd1256
[f92398f]: https://gitlab.com/gondolyr/mangadex-api/-/commit/f92398f

# Version 1.0.0-alpha.6 (2021-10-12)

All the new endpoint additions were added in MangaDex 5.3.4.

## Added

- ([d101a343]) Added `MangaState` enum to hold `state` field values. The new `state` field describes the staff approval status for a manga. When a manga is created, it requires staff approval for it to appear to the public.
- ([cca78131], [3476da18]) Added the get a specific Manga Draft endpoint (`GET /manga/draft/{id}`). This allows users to fetch a specific manga that isn't published.
- ([80929934], [3476da18]) Added submit Manga Draft endpoint (`POST /manga/draft/{id}/commit`). This allows users to change the state of a "draft" Manga to "submitted" for staff approval.
- ([039bdd29], [3476da18]) Added search Manga Drafts endpoint (`GET /manga/draft`). This allows users to search for Manga that have not been published.
- ([c8f2b1bd]) Added Manga relation list endpoint (`GET /manga/{id}/relation`). This endpoint fetches the related manga for a specific Manga such as spin-offs and sequels.
- ([bf74e422]) Added create a Manga relation endpoint (`POST /manga/{id}/relation`). This endpoint adds a relationship between Manga such as specifying one is a spin-off of the main story.
- ([c3503f43]) Added delete a Manga relation endpoint (`DELETE /manga/{id}/relation`). This endpoint removes the relationship between Manga.

## Changed

- ([fdcc8b89]) [**BREAKING**] Renamed `MangaRelated` enum to `MangaRelation`.
This enum wasn't really used outside of a single GET endpoint, so I don't expect much use of this directly.
With how the new MangaDex 5.3.4 endpoints are using this enum, it makes
more sense to call it a Manga relation, semantically speaking, because
it is a noun describing the entity, not a verb.
While this is bikeshedding, I feel more comfortable with this naming and prefer to
make this breaking change now rather than later when it is now used for
more than just a field type from a response.

## Internal

- ([a8401d8e]) Removed the ignore attribute from the list HTTP 400 tests. These tests are now run when the project tests are run.
- ([9990ccec]) Removed unused `Serialize`/`Deserialize` traits from `CreateManga` request struct and `NoData` struct.

[039bdd29]: https://gitlab.com/gondolyr/mangadex-api/-/commit/039bdd29
[3476da18]: https://gitlab.com/gondolyr/mangadex-api/-/commit/3476da18
[80929934]: https://gitlab.com/gondolyr/mangadex-api/-/commit/80929934
[9990ccec]: https://gitlab.com/gondolyr/mangadex-api/-/commit/9990ccec
[a8401d8e]: https://gitlab.com/gondolyr/mangadex-api/-/commit/a8401d8e
[bf74e422]: https://gitlab.com/gondolyr/mangadex-api/-/commit/bf74e422
[c3503f43]: https://gitlab.com/gondolyr/mangadex-api/-/commit/c3503f43
[c8f2b1bd]: https://gitlab.com/gondolyr/mangadex-api/-/commit/c8f2b1bd
[cca78131]: https://gitlab.com/gondolyr/mangadex-api/-/commit/cca78131
[d101a343]: https://gitlab.com/gondolyr/mangadex-api/-/commit/d101a343
[fdcc8b89]: https://gitlab.com/gondolyr/mangadex-api/-/commit/fdcc8b89

# Version 1.0.0-alpha.5 (2021-10-06)

## Added

- ([da763d39]) Added the `focusLanguage` query parameter and response body field to the scanlation group list and update endpoint. The field was added in MangaDex 5.3.1.
- ([8cdbb45f]) Added the `altNames` field to the scanlation group response. The field was added in MangaDex 5.3.2.
- ([50e3444b]) Added social media fields to the author response body struct. These fields were added in MangaDex 5.3.2.
- ([48b0f8f3]) Added social media fields to the author create and update endpoint request body structs. These fields were added in MangaDex 5.3.2.
- ([2b520c6e]) Added `related` field to the `Relationship` struct. This field was added in MangaDex 5.3.3 and only appears for Manga entities and Manga relationships.
- ([6a114dd0]) Added the relationship's `attributes` field when [Reference Expansion](https://api.mangadex.org/docs.html#section/Reference-Expansion) is used.

## Changed

- ([28faf433]) Pass the underlying error context to the library's error context message. For example, if there was a `Reqwest` error, the `Reqwest` error message will be passed along with this library's custom error message to provide better transparency into what went wrong.

## Fixed

- ([828db141], [7debff8a] - Thanks @evetsso) Fixed how datetime fields are serialized with requests to match the `YYYY-MM-DDTHH:MM:SS` format the MangaDex API is expectation. The `chrono` library serializes to RFC3339/ISO 8601 by default, which is not the format MangaDex accepts.

## Internal

- ([ec02dce9]) Added a `rustfmt.toml` configuration to the project to ensure Rust code is formatted to a specific standard.
- ([037719ce]) Added a cargo-deny config (`deny.toml`) to ensure dependencies are secure and currently maintained. For now, this isn't used in the CI/CD pipeline due to the `ring` dependency being unmaintained from this library's use of `rustls`; there are also license parsing errors in some dependencies due to the projects' multi-license setup.
- ([9ce6444c]) Added the `target/` directory to the GitLab CI/CD cache to improve the CI/CD performance.
- ([43047625]) Removed the `Serialize` trait from the response structs. This should slightly improve compile-time performance and slightly reduce the binary size, though not in a significant way.

[037719ce]: https://gitlab.com/gondolyr/mangadex-api/-/commit/037719ce
[28faf433]: https://gitlab.com/gondolyr/mangadex-api/-/commit/28faf433
[2b520c6e]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2b520c6e
[43047625]: https://gitlab.com/gondolyr/mangadex-api/-/commit/43047625
[48b0f8f3]: https://gitlab.com/gondolyr/mangadex-api/-/commit/48b0f8f3
[50e3444b]: https://gitlab.com/gondolyr/mangadex-api/-/commit/50e3444b
[6a114dd0]: https://gitlab.com/gondolyr/mangadex-api/-/commit/6a114dd0
[7debff8a]: https://gitlab.com/gondolyr/mangadex-api/-/commit/7debff8a
[828db141]: https://gitlab.com/gondolyr/mangadex-api/-/commit/828db141
[8cdbb45f]: https://gitlab.com/gondolyr/mangadex-api/-/commit/8cdbb45f
[9ce6444c]: https://gitlab.com/gondolyr/mangadex-api/-/commit/9ce6444c
[da763d39]: https://gitlab.com/gondolyr/mangadex-api/-/commit/da763d39
[ec02dce9]: https://gitlab.com/gondolyr/mangadex-api/-/commit/ec02dce9

# Version 1.0.0-alpha.4 (2021-09-15)

This release fixes the inconsistent list response body structure that the MangaDex API returned prior to MangaDex 5.2.35.
Many breaking changes have been introduced, primarily to the list endpoints.

## Added

- [Add `includes[]` query parameter to the user followed groups endpoint (`GET /user/follows/group`).][d3b8baff]
- [Add `includes[]` query parameter to the user followed manga endpoint (`GET /user/follows/manga`).][2117471b]
- [Add `followedCount` and `relevance` sort categories for the manga list endpoint.][b3fc3c78]
- [Add the batch mark manga chapters endpoint (`POST /manga/{id}/read`).][fe55aafa]
- [Add the `id` field for the chapter aggregate response struct (`GET /manga/{id}/aggregate`).][b6bbfe12]
- [Add `includeFutureUpdates` query parameter to the chapter list endpoint (`GET /chapter`).][bc119da2]
- [Add `includeFutureUpdates` query parameter to the user followed manga feed endpoint (`GET /user/follows/manga/feed`).][c868ec44]
- [Add `includeFutureUpdates` query parameter to the custom list manga feed endpoint (`GET /list/{id}/feed`).][25b44425]
- [Add `includeFutureUpdates` query parameter to the manga feed endpoint (`GET /manga/{id}/feed`).][3dd833d3]
- [Add `result` field to the response struct for the AtHome server endpoint (`GET /at-home/server/{id}`).][37c994f1]

## Changed

- \[**BREAKING**\] [MangaDex now returns list responses in the same structure as single lookups as of MangaDex 5.2.35. The following endpoints have been affected:][d0a0fa2d]
    - List/search authors (`GET /author`)
    - List/search chapters (`GET /chapter`)
    - List/search covers (`GET /cover`)
    - List/search custom lists (`GET /list`)
    - List/search manga (`GET /manga`)
    - List/search scanlation groups (`GET /group`)
    - List/search users (`GET /user`)
    - Custom list manga feed (`GET /list/{id}/feed`)
    - Legacy ID mapping (`POST /legacy/mapping`)
    - Manga feed (`GET /manga/{id}/feed`)
    - List manga tags (`GET /manga/tag`)
    - List report reasons (`GET /report/reasons/{category}`)
    - List user custom lists (`GET /user/{id}/list`)
    - List my followed groups (`GET /user/follows/group`)
    - List my followed manga (`GET /user/follows/manga`)
    - List my followed manga feed (`GET /user/follows/manga/feed`)
    - List my followed users (`GET /user/follows/user`)
    - List my custom lists (`GET /user/list`)

[2117471b]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2117471b
[25b44425]: https://gitlab.com/gondolyr/mangadex-api/-/commit/25b44425
[37c994f1]: https://gitlab.com/gondolyr/mangadex-api/-/commit/37c994f1
[3dd833d3]: https://gitlab.com/gondolyr/mangadex-api/-/commit/3dd833d3
[b3fc3c78]: https://gitlab.com/gondolyr/mangadex-api/-/commit/b3fc3c78
[b6bbfe12]: https://gitlab.com/gondolyr/mangadex-api/-/commit/b6bbfe12
[bc119da2]: https://gitlab.com/gondolyr/mangadex-api/-/commit/bc119da2
[c868ec44]: https://gitlab.com/gondolyr/mangadex-api/-/commit/c868ec44
[d0a0fa2d]: https://gitlab.com/gondolyr/mangadex-api/-/commit/d0a0fa2d
[d3b8baff]: https://gitlab.com/gondolyr/mangadex-api/-/commit/d3b8baff
[fe55aafa]: https://gitlab.com/gondolyr/mangadex-api/-/commit/fe55aafa

# Version 1.0.0-alpha.3 (2021-09-01)

This version addresses the breaking changes in MangaDex 5.2.24.
The biggest changes are to the create scanlation group request fields and the move of the `relationships` JSON response field from the top-level response to inside the `data` field.

## Added

- [Added the `groups[]` query parameter to the aggregate manga endpoint (Get manga volumes & chapters)][6db1f424]

## Changed

- \[**BREAKING**\] [MangaDex now returns the `relationships` array inside the `data` object. The following endpoints have had their response structs adjusted:][a40aad35]
    - Create an author (`POST /author`)
    - Get an author (`GET /author/{id}`)
    - List/search authors (`GET /author`)
    - Update an authors (`PUT /author/{id}`)
    - Get a chapter (`GET /chapter/{id}`)
    - List/search chapters (`GET /chapter`)
    - Update a chapter (`PUT /chapter/{id}`)
    - Edit a cover (`PUT /cover/{id}`)
    - Get a cover (`GET /cover/{id}`)
    - List/search covers (`GET /cover`)
    - Upload a cover (`POST /cover`)
    - Create a custom list (`POST /list`)
    - Get a custom list (`GET /list/{id}`)
    - Get a custom list manga feed (`GET /list/{id}/feed`)
    - Update a custom list (`PUT /list/{id}`)
    - Get legacy ID mapping (`POST /legacy/mapping`)
    - Create a manga (`POST /manga`)
    - Manga feed (`GET /manga/{id}/feed`)
    - Get a manga (`GET /manga/{id}`)
    - List/search manga (`GET /manga`)
    - List manga tags (`GET /manga/tag`)
    - Get a random manga (`GET /manga/random`)
    - Update a manga (`PUT /manga/{id}`)
    - Create a scanlation group (`POST /group`)
    - Get a scanlation group (`GET /group/{id}`)
    - List/search scanlation groups (`GET /group`)
    - Update a scanlation group (`PUT /group/{id}`)
    - Commit an upload session (`PUT /upload/{id}/commit`)
    - Upload images to an upload session (`POST /upload/{id}`)
    - Get the logged-in user's followed groups (`GET /user/follows/group`)
    - Get the logged-in user's followed manga (`GET /user/follows/manga`)
    - Get the logged-in user's followed manga feed (`GET /user/follows/manga/feed`)
- \[**BREAKING**\] [Updated the create scanlation group request body fields. MangaDex updated the request body fields in 5.2.24.][2f08cde1]

## Internal

- [Rename the `v5_cli` example to `v5_demo`. The use of CLI implied that it did more than simply demonstrate a couple of fetch endpoint and print the response bodies.][c1fbfe68]

[2f08cde1]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2f08cde1
[6db1f424]: https://gitlab.com/gondolyr/mangadex-api/-/commit/6db1f424
[a40aad35]: https://gitlab.com/gondolyr/mangadex-api/-/commit/a40aad35
[c1fbfe68]: https://gitlab.com/gondolyr/mangadex-api/-/commit/c1fbfe68

# Version 1.0.0-alpha.2 (2021-08-31)

This version addresses the changes introduced in MangaDex 5.2.23.
The most notable change in this version of MangaDex is the additional language query parameters such as `originalLanguage[]` and `excludedOriginalLanguage[]`.

## Added

- [The `excludedTranslatedLanguage[]` and `availableTranslatedLanguage[]` query parameters were added to the list manga endpoint.][4279a416]
- [The `originalLanguage[]` and `excludedOriginalLanguage[]` query parameters were added to the list chapter endpoint.][c9f686ea]
- [The `originalLanguage[]` and `excludedOriginalLanguage[]` query parameters were added to the user followed manga feed endpoint.][14ca38a9]
- [The `originalLanguage[]` and `excludedOriginalLanguage[]` query parameters were added to the CustomList manga feed endpoint.][2cc5c7fa]
- [The `originalLanguage[]` and `excludedOriginalLanguage[]` query parameters were added to the manga feed endpoint.][67650978]

## Changed

- \[**BREAKING**\] [The manga response (`MangaAttributes`) `status` field is no longer marked as nullable according to the MangaDex docs. The `Option` wrapper has been removed as a result.][ab72edd0]
- \[**BREAKING**\] [The `originalLanguage`, `status`, and `contentRating` fields for creating a new manga entry are now required by MangaDex.][00ffd568]

## Fixed

- [The `TagSearchMode` enum now serializes into uppercase as the MangaDex API requires it to be.][83b60431]

## Internal

- [The sort order enums now use a macro to define them. All of the enums' variants are tuple structs that use the `OrderDirection` enum so using a macro ensures that every enum is consistent.][2b30cb93]

[00ffd568]: https://gitlab.com/gondolyr/mangadex-api/-/commit/00ffd568
[14ca38a9]: https://gitlab.com/gondolyr/mangadex-api/-/commit/14ca38a9
[2b30cb93]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2b30cb93
[2cc5c7fa]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2cc5c7fa
[4279a416]: https://gitlab.com/gondolyr/mangadex-api/-/commit/4279a416
[67650978]: https://gitlab.com/gondolyr/mangadex-api/-/commit/67650978
[83b60431]: https://gitlab.com/gondolyr/mangadex-api/-/commit/83b60431
[ab72edd0]: https://gitlab.com/gondolyr/mangadex-api/-/commit/ab72edd0
[c9f686ea]: https://gitlab.com/gondolyr/mangadex-api/-/commit/c9f686ea

# Version 1.0.0-alpha.1 (2021-08-30)

The main additions to this version are multi-threaded support and support for the new fields in the query parameters, request bodies, and response bodies added after 5.2.20.
With multi-threaded support, you can use this library in applications that require the use of multiple threads such as GUI applications. Just enable the `multi-thread` feature, and you will be able to make async requests in multiple threads.

## Added

- [Error messages have been modified to use all lowercase and no ending punctuation to respect the idea that "you are not necessarily the start of the sentence" because the returned error may be printed as the cause of some other error.][711421eb]
- [Add support for multi-threaded operations by using `Arc` and `Mutex`.][e329e6f5] [This is hidden behind the `multi-thread` feature and is not enabled by default because it is experimental and also has a very minor performance hit compared to using `Rc` and `RefCell`.][c7a727bf]
- [Add the `contentRating[]` query parameter to the chapter list endpoint.][4a6cd87d]
- [Add the `contentRating[]` and `includes[]` query parameters to the user manga feed endpoint.][897314e9]
- [Add the `contentRating[]` and `includes[]` query parameters to the CustomList manga feed endpoint.][8c2f47ce]
- [Add the `volume` field to the upload cover endpoint request body.][db8b8171]
- [Add the `contentRating[]` query parameter to the manga feed endpoint.][de6521c9]
- [Add `official` and `verified` scanlation group response fields (`ScanlationGroupAttributes`). The `verified` field is not included in the API documentation as of 5.2.22 but is returned in the response body.][8a9483d5]
- [Add the `roles` field to the user response fields (`UserAttributes`). A `UserRole` enum has been started but because not all of the different variants are known, it is currently acting as a placeholder with the current discovered values.][7b21d2c2]
- [Add the `derive_builder::UninitializedFieldError` error as an internal error variant so that `.build()?` can be used if the `mangadex_api` `Error` enum is used in an application as a return type.][b657005e]

## Changed

- [The `MangaStatus` enum `Display` implementation now outputs title-case (e.g. "This is Title Case") for flexible usability. Some applications may want to print out the status from a response and this makes it convenient to do so.][ebfcb5de]

## Documentation

- [The `lib.rs` module docblock has been moved over to the `README.md` file and now imports the `README.md` file so that the library's frontpage documentation is centralized.][f8c9c3ab]

## Internal

- [A type alias for the `HttpClient` reference counter has been added. This makes it easier to maintain the endpoints that use this so that it is centralized to a single location.][26164628]

[26164628]: https://gitlab.com/gondolyr/mangadex-api/-/commit/26164628
[4a6cd87d]: https://gitlab.com/gondolyr/mangadex-api/-/commit/4a6cd87d
[711421eb]: https://gitlab.com/gondolyr/mangadex-api/-/commit/711421eb
[7b21d2c2]: https://gitlab.com/gondolyr/mangadex-api/-/commit/7b21d2c2
[897314e9]: https://gitlab.com/gondolyr/mangadex-api/-/commit/897314e9
[8a9483d5]: https://gitlab.com/gondolyr/mangadex-api/-/commit/8a9483d5
[8c2f47ce]: https://gitlab.com/gondolyr/mangadex-api/-/commit/8c2f47ce
[b657005e]: https://gitlab.com/gondolyr/mangadex-api/-/commit/b657005e
[c7a727bf]: https://gitlab.com/gondolyr/mangadex-api/-/commit/c7a727bf
[db8b8171]: https://gitlab.com/gondolyr/mangadex-api/-/commit/db8b8171
[de6521c9]: https://gitlab.com/gondolyr/mangadex-api/-/commit/de6521c9
[e329e6f5]: https://gitlab.com/gondolyr/mangadex-api/-/commit/e329e6f5
[ebfcb5de]: https://gitlab.com/gondolyr/mangadex-api/-/commit/ebfcb5de
[f8c9c3ab]: https://gitlab.com/gondolyr/mangadex-api/-/commit/f8c9c3ab

# Version 1.0.0-alpha.0 (2021-08-23)

I am pleased to announce the first public release of the rewrite of `mangadex-api`, which has feature parity with the [v5 MangaDex API]((https://api.mangadex.org/docs.html)!

The ergonomics of the library are still being adjusted but all of the available public endpoints (as of 5.2.20) are available for use with this library. While the library is in its 1.0.0 alpha state, small adjustments and fixes are expected before a 1.0.0 release.

A major change is the error handling where more descriptive errors are provided.

The "Upload" category is hidden behind the `upload` feature because it is experimental but should be overall functional.

Please report any issues or suggestions to the project at https://gitlab.com/gondolyr/mangadex-api/-/issues.

# Version 0.2.6 (2021-03-21)

## Changed

- [Add basic error handling when the MangaDex servers return an HTTP error code][ee414061]
    - This was discovered when MangaDex required extended maintenance time to resolve issues on their end.

[ee414061]: https://gitlab.com/gondolyr/mangadex-api/-/commit/ee414061c772bace7ed441b66ecb1a9186aac50c

# Version 0.2.5 (2021-03-11)

## Added

- [`Display` trait on the Demographic and PublicationStatus enums][c3744870]

## Changed

- [Blank manga artists are filtered out from the v2 `/manga/{id}` endpoint response so that consumers of this library don't have to do it themselves][0ca81a38]

## Documentation

- [Group `delay` field now has a more accurate description][172937df]
- [Clarified the `md_at_home` field's purpose][2ebd4663]
- [Added descriptions for manga response fields][1633138c]
- [Added a description for the `TagData.group` partial struct field][a7021282]

## Fixed

- [The `GroupChaptersResponse` struct now uses the `ChaptersDataPartial` struct for the `data` field to match the API's response structure][10ae6be7]
- [The `group_chapters()` v2 client builder method now uses the correct `GroupId` type alias for the `id` parameter][43800678]

[c3744870]: https://gitlab.com/gondolyr/mangadex-api/-/commit/c374487066ab2f3d151aa42ec394816876250180
[0ca81a38]: https://gitlab.com/gondolyr/mangadex-api/-/commit/0ca81a3807966846ca807066c8ebb20da22a98dc
[10ae6be7]: https://gitlab.com/gondolyr/mangadex-api/-/commit/10ae6be7b574470a1ddef7b9a30350fecdd7f0a9
[43800678]: https://gitlab.com/gondolyr/mangadex-api/-/commit/43800678508be9a84ae31e09748c43ede29b268b
[172937df]: https://gitlab.com/gondolyr/mangadex-api/-/commit/172937df8d13fa6498c3d00e0f6168e795e28e80
[2ebd4663]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2ebd4663bdb657c360ddb5f471d69ec8a4d6b790
[1633138c]: https://gitlab.com/gondolyr/mangadex-api/-/commit/1633138cbb974d57d3c64f10af3b9267d3d94044
[a7021282]: https://gitlab.com/gondolyr/mangadex-api/-/commit/a7021282112b3a7ca838b2f98ace14bdc2f84c3c

# Version 0.2.4 (2021-03-05)

## Changed

- [Web scrape clients include `Send + Sync` traits][f8008011]

## Documentation

- [Add flags to include all features and document private items when publishing to docs.rs][5a4f7a2f]

[5a4f7a2f]: https://gitlab.com/gondolyr/mangadex-api/-/commit/5a4f7a2f69c434aeeeb76a114ccce58613cf49db
[f8008011]: https://gitlab.com/gondolyr/mangadex-api/-/commit/f80080119bad511b9037f20249480c5d398fc53e

# Version 0.2.3 (2021-01-16)

## Added

- [`thread_id` field to `ChapterPartial` struct][1a6cf334]
- [Added `read` field to the v2 followed-updates endpoint response][409d61df]
- [Added `blockgroups` query parameter to all endpoints that return a list of chapters][c6e7e2db]. Enabling this filters out chapters belonging to groups that the user has blocked.
    - `/v2/group/{id}/chapters`
    - `/v2/manga/{id}/chapters`
    - `/v2/user/{id|me}/chapters`
    - `/v2/user/{id|me}/followed-updates`
- [Added `type` and `hentai` parameters to the v2 followed-manga endpoint][e8689022]. These are the same parameters that the followed-updates endpoint permitted.

## Changed

- [Base API URL now points to `api.mangadex.org`][c2870def]
- [The followed-updates response struct now uses the `title` field instead of `name`][6bdada80]. The `name()` getter has been preserved to maintain compatiblity for those using `name()`.

## Documentation

- [Add `threadId` field to the v2 OpenAPI document][a9284838]

## Fixed

- ["external" chapter status types are able to be parsed][48513b38]
- [The response struct for the v2 manga chapters endpoint (`/v2/manga/{id}/chapters`) has been fixed][71851fa5]. Previously, it was using the wrong struct for the `data` field.

[c2870def]: https://gitlab.com/gondolyr/mangadex-api/-/commit/c2870def1f9e509449e147f7de0d6abcf2274f0d
[6bdada80]: https://gitlab.com/gondolyr/mangadex-api/-/commit/6bdada801b9a9a829c4e46bd15f2872b20c3aa3c
[409d61df]: https://gitlab.com/gondolyr/mangadex-api/-/commit/409d61dfbbdb2657707819af10ef36f38d00e1d1
[48513b38]: https://gitlab.com/gondolyr/mangadex-api/-/commit/48513b385fa55580a683ccb8a48ff45327064860
[c6e7e2db]: https://gitlab.com/gondolyr/mangadex-api/-/commit/c6e7e2db7b5bc2356d5cdb2d8de7f3fca5dc376e
[e8689022]: https://gitlab.com/gondolyr/mangadex-api/-/commit/bd01b61ff69f2a3eb8ea39ab94ea331cce4ff52c
[71851fa5]: https://gitlab.com/gondolyr/mangadex-api/-/commit/71851fa5e27ee97ce8e14aa09bf86fc481d80c81
[1a6cf334]: https://gitlab.com/gondolyr/mangadex-api/-/commit/1a6cf334ef64800af22f7548d12531326b2dbc6c
[a9284838]: https://gitlab.com/gondolyr/mangadex-api/-/commit/a928483880bca18c72b5ca051858c4cd98e752f6

# Version 0.2.2 (2021-01-08)

## Dependencies

- [Update Reqwest to 0.11][b8e82c6]
- [Update Tokio to 1.0][b8e82c6]

[b8e82c6]: https://gitlab.com/gondolyr/mangadex-api/-/commit/b8e82c6d414717dabfa8c6650f6bf52f16c4dc99

# Version 0.2.0 (2020-12-29)

The web scraping search, fetch latest updates, and most popular manga functionality
all return a struct with the manga ID, title, and cover image URL.

## Added

- [Added `SiteTheme` enum for choosing the website theme][fc45db0]
- [Added functionality to fetch the latest manga updates via web scraping][bcfd464]
- [Added search functionality via web scraping][84216cb]
- [Added functionality to fetch the most popular manga (highest Bayesian rating) via web scraping][2d616df]
- [Added a `builder()` method to the `MangaDexV2` struct to allow for API configuration for things such as the user agent][f5874bd]
- [Added `tags()` method to `TagCategory` enum to return the associated tags with the category][0375fe6]

## Changed

- \[BREAKING\] [Changed `Language` default value to `English` because MangaDex appears to default to this language][9804cab]
- \[BREAKING\] [Make "time"/"Chrono" feature on by default to make it easier to work with dates and times][66845e9]
- [Changed enum function arguments to accept generic values for improved flexibility][8f296ef]
- [Make `Language` enum `From` trait implementations case-insensitive][4b522b9]

## Examples

- [Added login example to README][2705240]

## Fixed

- [Fixed `Language::Other` integer value][9804cab]

## Internal Only

- [Implement `FromStr` trait on Language enum][2c4d698]
- [Removed patch versions from Cargo.toml because the latest patch fixes should not include any breaking
  changes and should instead bring the latest fixes][2891a55]

[2c4d698]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2c4d69860f729579bcffbfaacdd11c3bf0d79421
[fc45db0]: https://gitlab.com/gondolyr/mangadex-api/-/commit/fc45db08c500cec8a665351a4df3dd90e80f664a
[9804cab]: https://gitlab.com/gondolyr/mangadex-api/-/commit/9804cab889284de1a463bedf8fd6e33fa945930d
[bcfd464]: https://gitlab.com/gondolyr/mangadex-api/-/commit/bcfd4641c9ad74b88c7c2783fdb2af6e1776f4d6
[84216cb]: https://gitlab.com/gondolyr/mangadex-api/-/commit/84216cbe3a930bdf6db1f271c1dd0749d42a0b9a
[8f296ef]: https://gitlab.com/gondolyr/mangadex-api/-/commit/8f296efaa8a0583bf9722229de773bd8abfb468b
[66845e9]: https://gitlab.com/gondolyr/mangadex-api/-/commit/66845e9dc3fffed4289353bcf7e68f002f65343b
[2d616df]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2d616dfdab870eacb9e1b348b2c6cf1da129c0b6
[f5874bd]: https://gitlab.com/gondolyr/mangadex-api/-/commit/f5874bd6e5fb826e99beec7cd56a89b86c917678
[4b522b9]: https://gitlab.com/gondolyr/mangadex-api/-/commit/4b522b9adf8d8609e8f64a3943a49db484db882d
[2891a55]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2891a55a1991916b25d1c7118fe6c038216fa0ec
[0375fe6]: https://gitlab.com/gondolyr/mangadex-api/-/commit/0375fe6fc46343ff4a07a38b1053c6aeb9d3819c
[2705240]: https://gitlab.com/gondolyr/mangadex-api/-/commit/27052400efdc3feecb6c3ce56c5505764cbf01d5

# Version 0.1.1 (2020-12-11)

## Improvements

- [Added `Copy` trait to internal MangaDex types.][ac533c0a]
- [Added `Hash` and `Serialize` to internal MangaDex types.][135f2c69]

## Documentation

- [Added GitLab pages job to publish Cargo-generated docs onto project website.][4d9065e1]
- [Fixed README example and v2 client docblocks return types.][f2197738]
- [Added Contributing guide.][21da4ca6]

## Examples

- [Added manga cover image downloader.][e9980599]
- [Added chapter downloader.][2f04b936]

## Internal Only

- [Moved `TagCategory` out of `tag` module to `tag_category`.][883b0e15]
- [Removed `getset` crate and implemented own getters.][da206ffc]

[4d9065e1]: https://gitlab.com/gondolyr/mangadex-api/-/commit/4d9065e180c87f1bd4be781171714b6d52e2f9ed
[f2197738]: https://gitlab.com/gondolyr/mangadex-api/-/commit/f219773828538783267eed7cf0534724735566c9
[21da4ca6]: https://gitlab.com/gondolyr/mangadex-api/-/commit/21da4ca6c12f282b086a835760e9616174242b67
[e9980599]: https://gitlab.com/gondolyr/mangadex-api/-/commit/e9980599c50073d621edc31e85cd6a6c89764788
[ac533c0a]: https://gitlab.com/gondolyr/mangadex-api/-/commit/ac533c0a05183d4e678387dd148551a4bc575fdf
[883b0e15]: https://gitlab.com/gondolyr/mangadex-api/-/commit/883b0e158bcc8835be075c10803c42362c5f3fe1
[135f2c69]: https://gitlab.com/gondolyr/mangadex-api/-/commit/135f2c69b39ea304ed0e29e3b0f301c668d08c6d
[2f04b936]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2f04b93667390b28729d910759f206670819780b
[da206ffc]: https://gitlab.com/gondolyr/mangadex-api/-/commit/da206ffca29657173dd1f28e2a475468657fc9b2

# Version 0.1.0 (2020-12-06)

## Features

- [All (most) MangaDex v2 endpoints implemented as of 2020-12-06.][endpoints]
  - API index page is not implemented. At this time, there is no intention to implement this unless there is strong demand.
- [Can log in to access restricted endpoints.][d8f794f8]
- [Chrono is a feature that will transmute various date/time fields as Chrono types][9d71c29b]
- [Example CLI tool for calling all the implemented v2 endpoints.][6c1f024c]
    - This does not have a lot of customization as it is designed to demonstrate basic usage.
- [Many MangaDex types have been internalized to Rust enums and structs for ease-of-use.][types]

[9d71c29b]: https://gitlab.com/gondolyr/mangadex-api/-/commit/9d71c29b3ca41201fe89b3fd351cc8792a522522
[6c1f024c]: https://gitlab.com/gondolyr/mangadex-api/-/commit/6c1f024cde9cd396e0fd0b260b0f82c455eccd40
[d8f794f8]: https://gitlab.com/gondolyr/mangadex-api/-/commit/d8f794f860f8b93c306fd52e7ba61d013c88bc1f
[endpoints]: https://gitlab.com/gondolyr/mangadex-api/-/tree/0.1.0/src/v2/builder
[types]: https://gitlab.com/gondolyr/mangadex-api/-/tree/0.1.0/src/types
